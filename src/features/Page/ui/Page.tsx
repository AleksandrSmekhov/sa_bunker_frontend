import { useEffect } from 'react';
import { Outlet, useNavigate, useLocation } from 'react-router';

import { RoutePath } from 'shared/consts/router';

import cls from './Page.module.scss';

export const Page = () => {
  const { pathname } = useLocation();
  const navigate = useNavigate();

  useEffect(() => {
    if (pathname === '/') navigate(RoutePath.menu);
  }, [navigate, pathname]);

  return (
    <div className={cls.content}>
      <Outlet />
    </div>
  );
};
