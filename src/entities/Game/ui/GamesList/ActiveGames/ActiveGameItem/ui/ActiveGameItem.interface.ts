import type { IActiveGame } from 'entities/Game/model/interfaces/IGetGames.interface';

export interface IActiveGameItemProps {
  game: IActiveGame;
  lineStyle: string;
}
