import { useMemo } from 'react';
import { useDispatch } from 'react-redux';
import { bindActionCreators } from '@reduxjs/toolkit';

import { gamesSliceActions } from 'shared/slices';

export const useGames = () => {
  const dispatch = useDispatch();

  return useMemo(
    () => bindActionCreators(gamesSliceActions, dispatch),
    [dispatch]
  );
};
