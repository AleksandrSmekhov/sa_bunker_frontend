import type { IPlayer } from 'entities/Game/model/interfaces/IGetGame.interface';

import type { PlayerCardTypes } from 'entities/Card/model/consts/enums';

export interface IPlayerItemProps {
  player: IPlayer;
}

export interface ICardIconsValues {
  type: PlayerCardTypes;
  activeStyle: string;
  inActiveStyle: string;
  key: string;
  icon: React.FC<React.SVGProps<SVGSVGElement>>;
}
