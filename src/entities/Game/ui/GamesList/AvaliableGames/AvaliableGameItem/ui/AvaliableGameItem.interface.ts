import type { IAvaliableGame } from 'entities/Game/model/interfaces/IGetGames.interface';

export interface IAvaliableGameItemProps {
  game: IAvaliableGame;
  lineStyle: string;
  isAdmin: boolean;
  setGameId: React.Dispatch<React.SetStateAction<string>>;
  modalRef: React.RefObject<HTMLDialogElement>;
}
