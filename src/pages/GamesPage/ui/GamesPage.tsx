import { useGetGames } from 'entities/Game';

import { ReactComponent as Skull } from 'shared/assets/icons/skull.svg';
import { Window } from 'features/Window';
import { LoadingElement } from 'features/LoadingElement';
import { GamesList } from 'entities/Game';

import cls from './GamesPage.module.scss';

export const GamesPage = () => {
  const { isConnected, isError, socket } = useGetGames();

  const handleDisconnect = () => {
    socket.close();
  };

  if (!isConnected && !isError)
    return (
      <Window header="Игры">
        <LoadingElement />
      </Window>
    );

  if (isError)
    return (
      <Window header="Игры">
        <div className={cls.errorMessage}>
          <h1>Ошибка подключения</h1>
          <Skull />
        </div>
      </Window>
    );

  return (
    <Window header="Игры" extraExitFunc={handleDisconnect}>
      <GamesList />
    </Window>
  );
};
