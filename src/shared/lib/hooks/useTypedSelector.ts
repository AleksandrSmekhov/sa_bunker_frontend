import { TypedUseSelectorHook, useSelector } from 'react-redux';

import type { RootState } from 'app/providers/StoreProvider/config';

export const useTypedSelector: TypedUseSelectorHook<RootState> = useSelector;
