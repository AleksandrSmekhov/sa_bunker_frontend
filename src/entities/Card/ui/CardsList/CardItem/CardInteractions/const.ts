import { ReactComponent as Baggage } from 'shared/assets/icons/baggage.svg';
import { ReactComponent as Biology } from 'shared/assets/icons/biology.svg';
import { ReactComponent as Bunker } from 'shared/assets/icons/bunker.svg';
import { ReactComponent as Disaster } from 'shared/assets/icons/disaster.svg';
import { ReactComponent as Facts } from 'shared/assets/icons/facts.svg';
import { ReactComponent as Health } from 'shared/assets/icons/health.svg';
import { ReactComponent as Hobby } from 'shared/assets/icons/hobby.svg';
import { ReactComponent as Profession } from 'shared/assets/icons/proffession.svg';
import { ReactComponent as Risk } from 'shared/assets/icons/risk.svg';

import type { ISectionValues, IIcons } from './ui/CardInteractions.interface';

import { InteractionTypes } from 'entities/Card/model/consts/enums';

import cls from './ui/styles/CardInteractions.module.scss';

export const SECTION_VALUES: ISectionValues[] = [
  {
    cardField: 'positive',
    title: 'Позитивные взаимодействия:',
    key: 'positive-card-interaction',
    interactionType: InteractionTypes.positive,
  },
  {
    cardField: 'negative',
    title: 'Негативные взаимодействия:',
    key: 'negative-card-interaction',
    interactionType: InteractionTypes.negative,
  },
];

export const ICONS: IIcons = {
  BAGGAGE: { icon: Baggage, style: cls.baggage },
  BIOLOGY: { icon: Biology, style: cls.biology },
  BUNKER: { icon: Bunker, style: cls.bunker },
  DISASTER: { icon: Disaster, style: cls.disaster },
  FACTS: { icon: Facts, style: cls.facts },
  HEALTH: { icon: Health, style: cls.health },
  HOBBY: { icon: Hobby, style: cls.hobby },
  PROFESSIONS: { icon: Profession, style: cls.profession },
  RISK: { icon: Risk, style: cls.risk },
};
