import type {
  IGameCard,
  IPlayerCard,
} from 'entities/Card/model/interfaces/IGetCards.interface';
import type {
  InteractionTypes,
  GameCardTypes,
  PlayerCardTypes,
} from 'entities/Card/model/consts/enums';

type ICard = IGameCard | IPlayerCard;

export interface ICardInteractionsProps {
  card: ICard;
  isAdmin: boolean;
}

export interface ISectionValues {
  cardField: keyof Pick<ICard, 'negative' | 'positive'>;
  title: string;
  key: string;
  interactionType: InteractionTypes;
}

export interface IAddInteractionProps {
  cardId: string;
  type: InteractionTypes;
}

interface IIcon {
  icon: React.FC<React.SVGProps<SVGSVGElement>>;
  style: string;
}

export type IIcons = Record<GameCardTypes | PlayerCardTypes, IIcon>;
