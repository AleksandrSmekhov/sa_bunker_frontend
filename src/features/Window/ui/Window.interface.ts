import type { PropsWithChildren } from 'react';

export interface IWindowProps extends PropsWithChildren {
  header: string;
  extraExitFunc?: () => void;
  extraWindowCls?: string;
}
