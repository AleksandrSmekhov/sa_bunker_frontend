import { useState } from 'react';
import { useNavigate } from 'react-router-dom';

import { useJoinGame } from 'entities/Game/model/GameApi';

import { ReactComponent as Cross } from 'shared/assets/icons/cross.svg';
import { ReactComponent as Enter } from 'shared/assets/icons/exit.svg';

import type { IEnterPasswordModalProps } from './EnterPasswordModal.interface';

import { RoutePath } from 'shared/consts/router';

import cls from './EnterPasswordModal.module.scss';

export const EnterPasswordModal = ({
  gameId,
  modalRef,
  setGameId,
}: IEnterPasswordModalProps) => {
  const [password, setPassword] = useState<string>('');
  const [isError, setIsError] = useState<boolean>(false);
  const navigate = useNavigate();
  const [joinGame] = useJoinGame();

  const handleClose = () => {
    modalRef.current?.close();
    setGameId('');
  };

  const handlePreventClosing = (
    event: React.MouseEvent<HTMLDivElement, MouseEvent>
  ) => {
    event.stopPropagation();
  };

  const handleEnterPassword = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (isError && event.target.value) setIsError(false);
    setPassword(event.target.value);
  };

  const handleEnter = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    joinGame({ game_id: gameId, password: password })
      .unwrap()
      .then(() => navigate(`${RoutePath.game}${gameId}`))
      .catch(() => setIsError(true));
  };

  return (
    <dialog onClick={handleClose} ref={modalRef} className={cls.modal}>
      <div onClick={handlePreventClosing} className={cls.container}>
        <header className={cls.header}>
          <h3>Вход в игру</h3>
          <Cross className={cls.close} onClick={handleClose} />
        </header>
        <section className={cls.content}>
          <form className={cls.form} onSubmit={handleEnter}>
            <div className={cls.input}>
              <label htmlFor="gamePasswordInput">Введите пароль:</label>
              <input
                id="gamePasswordInput"
                autoComplete="off"
                value={password}
                onChange={handleEnterPassword}
                type="password"
                placeholder="Пароль"
                className={isError ? cls.error : ''}
              ></input>
            </div>
            {isError ? (
              <p className={cls.wrongPassword}>Неверные пароль</p>
            ) : (
              <></>
            )}
            <button type="submit">
              Войти
              <Enter />
            </button>
          </form>
        </section>
      </div>
    </dialog>
  );
};
