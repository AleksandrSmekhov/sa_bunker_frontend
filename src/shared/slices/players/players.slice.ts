import { createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';

import type { IGetPlayersResult } from 'entities/Player/model/interfaces/IGetPlayers.interface';

import { PLAYERS_SLICE_NAME, INITIAL_VALUES } from './const';

export const playersSlice = createSlice({
  name: PLAYERS_SLICE_NAME,
  initialState: INITIAL_VALUES,
  reducers: {
    setData(_, { payload }: PayloadAction<IGetPlayersResult>) {
      return payload;
    },
  },
});

export const playersSliceActions = playersSlice.actions;
