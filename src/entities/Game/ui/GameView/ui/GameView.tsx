import { useTypedSelector } from 'shared/lib/hooks';

import { UserInfo, PlayersInGameList } from 'entities/Player';
import { GameInfo } from '../GameInfo';
import { GameRisks } from '../GameRisks';

import cls from './GameView.module.scss';

export const GameView = () => {
  const { game } = useTypedSelector((state) => state.gameSlice);

  return (
    <div className={cls.gameView}>
      <GameInfo />
      <div
        className={`${cls.gameInfoWrapper} ${game.round ? cls.game : cls.wait}`}
      >
        <div className={cls.gameInfo}>
          {!game.round ? <></> : <GameRisks />}
          <PlayersInGameList />
        </div>
      </div>
      <UserInfo />
    </div>
  );
};
