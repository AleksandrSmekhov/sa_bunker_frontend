import { useRef, useState } from 'react';

import { AvaliableGameItem } from '../AvaliableGameItem';
import { Modal } from '../Modal';

import type { IAvaliableGamesProps } from './AvaliableGames.interface';

import cls from './AvaliableGames.module.scss';

export const AvaliableGames = ({
  games,
  isAdmin,
  styles,
}: IAvaliableGamesProps) => {
  const enterPasswordModal = useRef<HTMLDialogElement>(null);
  const [passwordGameId, setPasswordGameId] = useState<string>('');

  const handleOpenModal = () => {
    enterPasswordModal.current?.showModal();
  };

  return (
    <div className={styles.block}>
      <h2 className={styles.header}>Доступные игры</h2>
      {games.length ? (
        games.map((game) => (
          <AvaliableGameItem
            game={game}
            lineStyle={styles.game}
            key={game.id}
            isAdmin={isAdmin}
            setGameId={setPasswordGameId}
            modalRef={enterPasswordModal}
          />
        ))
      ) : (
        <h3 className={styles.noGames}>Нет доступных игр...</h3>
      )}
      <button className={cls.createGame} onClick={handleOpenModal}>
        Создать игру
      </button>
      <Modal
        gameId={passwordGameId}
        setGameId={setPasswordGameId}
        modalRef={enterPasswordModal}
      />
    </div>
  );
};
