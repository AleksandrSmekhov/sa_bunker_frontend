import type { PlayerCardTypes } from 'entities/Card/model/consts/enums';
import type { GameCardTypes } from 'entities/Card/model/consts/enums';
import type { IInteraction } from 'entities/Game/model/interfaces/IGetGame.interface';

export interface ICardInfoProps {
  type: PlayerCardTypes | GameCardTypes;
  name: string;
  description: string;
  isOpen: boolean;
  interactions?: IInteraction[];
}

export type ITypeTranslation = Record<PlayerCardTypes | GameCardTypes, string>;
export type ITypeIcon = Record<
  PlayerCardTypes | GameCardTypes,
  React.FC<React.SVGProps<SVGSVGElement>>
>;
export type IIconStyle = Record<PlayerCardTypes | GameCardTypes, string>;

export interface IFiltredInteractions {
  positiveInteractions: IInteraction[];
  negativeInteractions: IInteraction[];
}
