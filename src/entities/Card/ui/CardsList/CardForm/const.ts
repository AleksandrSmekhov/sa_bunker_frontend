import type { IFlags } from './ui/CardForm.interface';

export const INITIAL_FLAGS: IFlags = {
  isDescriptionError: false,
  isNameError: false,
};
