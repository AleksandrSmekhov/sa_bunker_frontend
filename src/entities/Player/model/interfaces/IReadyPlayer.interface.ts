export interface IReadyPlayerBody {
  game_id: string;
  player_id: string;
  is_ready: boolean;
}
