interface IGame {
  id: string;
  name: string;
  players: number;
}

export interface IActiveGame extends IGame {
  round: number;
  is_voting: boolean;
  disaster: string;
  current_player: number;
}

export interface IArchiveGame extends IGame {
  disaster: string;
}

export interface IAvaliableGame extends IGame {
  password: boolean;
}

export interface IGetGamesResult {
  active: IActiveGame[];
  archive: IArchiveGame[];
  avaliable: IAvaliableGame[];
}
