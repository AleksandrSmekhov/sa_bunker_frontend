import { useLocation, Navigate } from 'react-router';

import { USER_ID_STORAGE_NAME } from 'shared/consts/storageNames';
import { RoutePath } from 'shared/consts/router';

interface IRequireAuthProps {
  children: JSX.Element;
}

export const RequireAuth = ({ children }: IRequireAuthProps) => {
  const userId = localStorage.getItem(USER_ID_STORAGE_NAME);

  const location = useLocation();

  if (!userId)
    return <Navigate to={RoutePath.login} state={{ from: location }} replace />;

  return children;
};
