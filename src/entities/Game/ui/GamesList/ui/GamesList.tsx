import { useTypedSelector } from 'shared/lib/hooks';

import { ActiveGames } from '../ActiveGames';
import { ArchiveGames } from '../ArchiveGames';
import { AvaliableGames } from '../AvaliableGames';

import { IS_ADMIN_STORAGE_NAME } from 'shared/consts/storageNames';

import cls from './GamesList.module.scss';

export const GamesList = () => {
  const { active, archive, avaliable } = useTypedSelector(
    (state) => state.gamesSlice
  );
  const isAdmin = localStorage.getItem(IS_ADMIN_STORAGE_NAME);

  return (
    <div className={cls.gamesList}>
      {active.length ? <ActiveGames games={active} styles={cls} /> : <></>}
      <AvaliableGames games={avaliable} isAdmin={!!isAdmin} styles={cls} />
      {archive.length ? <ArchiveGames games={archive} styles={cls} /> : <></>}
    </div>
  );
};
