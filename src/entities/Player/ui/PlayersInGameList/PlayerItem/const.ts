import { ReactComponent as Health } from 'shared/assets/icons/health.svg';
import { ReactComponent as Biology } from 'shared/assets/icons/biology.svg';
import { ReactComponent as Proffession } from 'shared/assets/icons/proffession.svg';
import { ReactComponent as Baggage } from 'shared/assets/icons/baggage.svg';
import { ReactComponent as Facts } from 'shared/assets/icons/facts.svg';
import { ReactComponent as Hobby } from 'shared/assets/icons/hobby.svg';

import type { ICardIconsValues } from './ui/PlayerItem.interface';

import { PlayerCardTypes } from 'entities/Card/model/consts/enums';

import cls from './ui/PlayerItem.module.scss';

export const CARDS_ICONS_VALUES: ICardIconsValues[] = [
  {
    type: PlayerCardTypes.health,
    activeStyle: cls.health,
    inActiveStyle: cls.inactiveFill,
    icon: Health,
    key: 'health-card-icon',
  },
  {
    type: PlayerCardTypes.biology,
    activeStyle: cls.biology,
    inActiveStyle: cls.inactiveFill,
    icon: Biology,
    key: 'biology-card-icon',
  },
  {
    type: PlayerCardTypes.professions,
    activeStyle: cls.profession,
    inActiveStyle: cls.inactiveFill,
    icon: Proffession,
    key: 'professions-card-icon',
  },
  {
    type: PlayerCardTypes.baggage,
    activeStyle: cls.baggage,
    inActiveStyle: cls.inactiveFill,
    icon: Baggage,
    key: 'baggage-card-icon',
  },
  {
    type: PlayerCardTypes.facts,
    activeStyle: cls.facts,
    inActiveStyle: cls.inactiveStroke,
    icon: Facts,
    key: 'facts-card-icon',
  },
  {
    type: PlayerCardTypes.hobby,
    activeStyle: cls.hobby,
    inActiveStyle: cls.inactiveFill,
    icon: Hobby,
    key: 'hobby-card-icon',
  },
];
