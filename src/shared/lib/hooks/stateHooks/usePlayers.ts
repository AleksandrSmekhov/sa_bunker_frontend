import { useMemo } from 'react';
import { useDispatch } from 'react-redux';
import { bindActionCreators } from '@reduxjs/toolkit';

import { playersSliceActions } from 'shared/slices';

export const usePlayers = () => {
  const dispatch = useDispatch();

  return useMemo(
    () => bindActionCreators(playersSliceActions, dispatch),
    [dispatch]
  );
};
