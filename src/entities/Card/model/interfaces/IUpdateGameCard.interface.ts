import type { ICreateGameCardBody } from './ICreateGameCard.interface';

export interface IUpdateGameCardBody extends ICreateGameCardBody {
  id: string;
}
