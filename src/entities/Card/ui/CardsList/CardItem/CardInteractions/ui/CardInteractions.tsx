import { useDeleteCardsInteraction } from 'entities/Card/model/CardApi';

import { ReactComponent as Trash } from 'shared/assets/icons/trash.svg';
import { AddGameCardInteraction } from '../AddGameCardInteraction';
import { AddPlayerCardInteraction } from '../AddPlayerCardInteraction';

import type { ICardInteractionsProps } from './CardInteractions.interface';

import { PLAYER_LINK_CARD_KEY } from 'entities/Card/model/interfaces/IGetCards.interface';
import {
  GameCardTypes,
  PlayerCardTypes,
} from 'entities/Card/model/consts/enums';
import { SECTION_VALUES, ICONS } from '../const';

import cls from './styles/CardInteractions.module.scss';

export const CardInteractions = ({ card, isAdmin }: ICardInteractionsProps) => {
  const [deleteInteraction] = useDeleteCardsInteraction();
  const isGameCard = Object.values(GameCardTypes).includes(
    card.type as GameCardTypes
  );

  if (!isAdmin && !card.positive.length && !card.negative.length)
    return (
      <div className={cls.noInteractions}>
        <p>Взаимодействия отсутствуют...</p>
      </div>
    );

  const handleDelete = (id: string) => {
    deleteInteraction({ id });
  };

  const getIcon = (type: GameCardTypes | PlayerCardTypes) => {
    const Icon = ICONS[type].icon;
    return <Icon className={ICONS[type].style} />;
  };

  return (
    <div className={cls.interactions}>
      {SECTION_VALUES.map((values) => (
        <div key={values.key} className={cls.section}>
          {card[values.cardField].length || isAdmin ? (
            <>
              <h4 className={cls.header}>{values.title}</h4>
              {card[values.cardField].map((values) => (
                <div
                  key={
                    PLAYER_LINK_CARD_KEY in values
                      ? values.player_link_card
                      : values.game_link_card
                  }
                  className={cls.interaction}
                >
                  {getIcon(values.type)}
                  <p>{values.name}</p>
                  {isAdmin ? (
                    <Trash
                      className={cls.trash}
                      onClick={() =>
                        handleDelete(
                          PLAYER_LINK_CARD_KEY in values
                            ? values.player_link_card
                            : values.game_link_card
                        )
                      }
                    />
                  ) : (
                    <></>
                  )}
                </div>
              ))}
            </>
          ) : (
            <></>
          )}
          {isAdmin ? (
            isGameCard ? (
              <AddGameCardInteraction
                cardId={card.id}
                type={values.interactionType}
              />
            ) : (
              <AddPlayerCardInteraction
                cardId={card.id}
                type={values.interactionType}
              />
            )
          ) : (
            <></>
          )}
        </div>
      ))}
    </div>
  );
};
