export interface ICreateGameBody {
  name: string;
  password?: string;
}

export interface ICreateGameResult {
  id: string;
  name: string;
  password: string;
}
