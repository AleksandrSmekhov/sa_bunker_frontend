import type { ICreatePlayerCardBody } from './ICreatePlayerCard.interface';

export interface IUpdatePlayerCardBody extends ICreatePlayerCardBody {
  id: string;
}
