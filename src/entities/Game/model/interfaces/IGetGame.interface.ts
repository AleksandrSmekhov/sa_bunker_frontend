import {
  GameCardTypes,
  PlayerCardTypes,
  InteractionTypes,
} from 'entities/Card/model/consts/enums';

interface IDisaster {
  id: string;
  name: string;
  description: string;
}

interface IGame {
  id: string;
  name: string;
  current_player: number;
  is_voting: boolean;
  round: number;
  disaster?: IDisaster;
}

interface IRisk {
  id: string;
  is_open: boolean;
  type: GameCardTypes;
  name: string;
  description: string;
}

export interface IInteraction {
  type: InteractionTypes;
  game_card?: string;
}

interface ICard {
  id: string;
  is_open: boolean;
  name: string;
  description: string;
  type: PlayerCardTypes;
  interactions: IInteraction[];
}

export interface IPlayer {
  id: string;
  name: string;
  vote: string;
  is_ready: boolean;
  is_in_game: boolean;
  number: number;
  cards: ICard[];
}

export interface IGetGameResult {
  game: IGame;
  risks: IRisk[];
  players: IPlayer[];
  player: IPlayer;
}
