import { useDeletePlayer } from 'entities/Player';

import { ReactComponent as Trash } from 'shared/assets/icons/trash.svg';

import type { IPlayerItemProps } from './PlayerItem.interface';

import cls from './PlayerItem.module.scss';

export const PlayerItem = ({ index, isAdmin, player }: IPlayerItemProps) => {
  const [deletePlayer] = useDeletePlayer();

  const handleDelete = () => {
    deletePlayer({ id: player.id })
      .unwrap()
      .then(() => {})
      .catch(() => {});
  };

  return (
    <div
      className={`${cls.player} ${index % 2 ? cls.odd : cls.even} ${
        isAdmin ? cls.adminView : cls.playerView
      }`}
    >
      <p>{index}</p>
      <p>{player.name}</p>
      <p>{player.games}</p>
      {isAdmin ? <Trash onClick={handleDelete} /> : <></>}
    </div>
  );
};
