import { useNavigate } from 'react-router-dom';

import { useDeleteGame, useJoinGame } from 'entities/Game';

import { ReactComponent as Lock } from 'shared/assets/icons/lock.svg';
import { ReactComponent as Enter } from 'shared/assets/icons/exit.svg';
import { ReactComponent as Trash } from 'shared/assets/icons/trash.svg';

import { getPlayersText } from 'shared/lib/funcs';

import type { IAvaliableGameItemProps } from './AvaliableGameItem.interface';

import { RoutePath } from 'shared/consts/router';

import cls from './AvaliableGameItem.module.scss';

export const AvaliableGameItem = ({
  game,
  isAdmin,
  lineStyle,
  setGameId,
  modalRef,
}: IAvaliableGameItemProps) => {
  const navigate = useNavigate();
  const [deleteGame] = useDeleteGame();
  const [joinGame] = useJoinGame();

  const handleEnterGame = () => {
    if (game.password) {
      setGameId(game.id);
      modalRef.current?.showModal();
      return;
    }

    joinGame({ game_id: game.id })
      .unwrap()
      .then(() => navigate(`${RoutePath.game}${game.id}`))
      .catch(() => ({}));
  };

  const handleDeleteGame = () => {
    deleteGame({ id: game.id })
      .unwrap()
      .then(() => ({}))
      .catch(() => ({}));
  };

  return (
    <div className={lineStyle}>
      <div className={cls.name}>
        <p>{game.name}</p>
        {game.password ? <Lock /> : <></>}
      </div>
      <p className={cls.players}>
        {game.players} {getPlayersText(game.players)}
      </p>
      <div className={cls.buttons}>
        {isAdmin ? (
          <button className={cls.deleteButton} onClick={handleDeleteGame}>
            <Trash />
          </button>
        ) : (
          <></>
        )}
        <button className={cls.enterButton} onClick={handleEnterGame}>
          <Enter />
        </button>
      </div>
    </div>
  );
};
