export interface IEnterPasswordSectionProps {
  sectionStyle: string;
  gameId: string;
  password: string;
  setPassword: React.Dispatch<React.SetStateAction<string>>;
  isError: boolean;
  setIsError: React.Dispatch<React.SetStateAction<boolean>>;
}
