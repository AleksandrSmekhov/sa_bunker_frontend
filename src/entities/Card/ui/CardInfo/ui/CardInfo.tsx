import { ReactComponent as Plus } from 'shared/assets/icons/plus.svg';
import { ReactComponent as Minus } from 'shared/assets/icons/minus.svg';

import type {
  ICardInfoProps,
  IFiltredInteractions,
} from './CardInfo.interface';

import { InteractionTypes } from 'entities/Card/model/consts/enums';
import {
  TYPES_ICONS,
  TYPES_TRANSLATIONS,
  INACTIVE_ICON_STYLES,
  ACTIVE_STYLES,
} from '../const';

import cls from './CardInfo.module.scss';

export const CardInfo = ({
  description,
  isOpen,
  name,
  type,
  interactions,
}: ICardInfoProps) => {
  const Icon = TYPES_ICONS[type];

  if (!isOpen)
    return (
      <div className={`${cls.card} ${cls.notOpenedCard}`}>
        {<Icon className={INACTIVE_ICON_STYLES[type]} />}
        <p>{TYPES_TRANSLATIONS[type]}</p>
      </div>
    );

  const { negativeInteractions, positiveInteractions }: IFiltredInteractions =
    interactions
      ? interactions.reduce<IFiltredInteractions>(
          (result, interaction) => {
            if (interaction.game_card)
              interaction.type === InteractionTypes.positive
                ? result.positiveInteractions.push(interaction)
                : result.negativeInteractions.push(interaction);
            return result;
          },
          { negativeInteractions: [], positiveInteractions: [] }
        )
      : { negativeInteractions: [], positiveInteractions: [] };

  return (
    <details>
      <summary
        className={`${cls.card} ${cls.openedCard} ${ACTIVE_STYLES[type]}`}
      >
        {<Icon />}
        <p>{name}</p>
        {positiveInteractions.map((_, index) => (
          <Plus key={`positive-interaction-${index}`} className={cls.plus} />
        ))}
        {negativeInteractions.map((_, index) => (
          <Minus key={`negative-interaction-${index}`} className={cls.minus} />
        ))}
      </summary>
      <div className={cls.description}>
        {description.split('\n').map((line, index) => (
          <p key={`description-line-${index}`}>{line}</p>
        ))}
        {positiveInteractions.length ? (
          <div className={`${cls.interactions} ${cls.positive}`}>
            <h4>Позитивное влияние:</h4>
            {positiveInteractions.map((integraction, index) => (
              <div
                key={`positive-interaction-${index}`}
                className={cls.interactionLine}
              >
                <Plus />
                <p>{integraction.game_card}</p>
              </div>
            ))}
          </div>
        ) : (
          <></>
        )}
        {negativeInteractions.length ? (
          <div className={`${cls.interactions} ${cls.negative}`}>
            <h4>Негативное влияние:</h4>
            {negativeInteractions.map((integraction, index) => (
              <div
                key={`negative-interaction-${index}`}
                className={cls.interactionLine}
              >
                <Minus />
                <p>{integraction.game_card}</p>
              </div>
            ))}
          </div>
        ) : (
          <></>
        )}
      </div>
    </details>
  );
};
