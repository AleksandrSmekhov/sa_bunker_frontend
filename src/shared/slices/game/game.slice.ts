import { createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';

import type { IGetGameResult } from 'entities/Game/model/interfaces/IGetGame.interface';

import { GAME_SLICE_NAME, INITIAL_VALUES } from './const';

export const gameSlice = createSlice({
  name: GAME_SLICE_NAME,
  initialState: INITIAL_VALUES,
  reducers: {
    setData(_, { payload }: PayloadAction<IGetGameResult>) {
      return payload;
    },
  },
});

export const gameSliceActions = gameSlice.actions;
