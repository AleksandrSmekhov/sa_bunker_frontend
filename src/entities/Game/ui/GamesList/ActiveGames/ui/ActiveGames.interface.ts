import type { IActiveGame } from 'entities/Game/model/interfaces/IGetGames.interface';

export interface IActiveGamesProps {
  games: IActiveGame[];
  styles: Record<string, string>;
}
