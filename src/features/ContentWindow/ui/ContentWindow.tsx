import type { IContentWindowProps } from './ContentWindow.interface';

import cls from './ContentWindow.module.scss';

export const ContentWindow = ({
  children,
  header,
  extraBlockClass,
  extraBlockWrapperClass,
}: IContentWindowProps) => {
  return (
    <div className={cls.content}>
      <div
        className={`${cls.blockWrapper} ${
          header ? cls.blockWrapperWithHeader : ''
        } ${extraBlockWrapperClass || ''}`}
      >
        {header ? <h1 className={cls.header}>{header}</h1> : ''}
        <div className={`${cls.block} ${extraBlockClass || ''}`}>
          {children}
        </div>
      </div>
    </div>
  );
};
