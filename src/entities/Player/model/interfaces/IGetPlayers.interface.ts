export interface IGetPlayerValues {
  games: number;
  id: string;
  name: string;
}

export type IGetPlayersResult = IGetPlayerValues[];
