import type { PlayerCardTypes } from 'entities/Card/model/consts/enums';

export interface IOption {
  label: string;
  value: string;
  color?: string;
}

export type ITypeColor = Record<PlayerCardTypes, string>;
