import { useNavigate } from 'react-router-dom';

import { ContentWindow } from 'features/ContentWindow';

import { RoutePath } from 'shared/consts/router';
import {
  USER_ID_STORAGE_NAME,
  IS_ADMIN_STORAGE_NAME,
  USER_NAME_STORAGE_NAME,
} from 'shared/consts/storageNames';
import { MENU_ITEMS } from '../const';

import cls from './MenuPage.module.scss';

export const MenuPage = () => {
  const navigate = useNavigate();

  const handleChangePage = (path: string) => {
    if (path === RoutePath.login) {
      localStorage.removeItem(USER_ID_STORAGE_NAME);
      localStorage.removeItem(IS_ADMIN_STORAGE_NAME);
      localStorage.removeItem(USER_NAME_STORAGE_NAME);
    }

    navigate(path);
  };

  return (
    <ContentWindow
      header="Меню"
      extraBlockClass={cls.block}
      extraBlockWrapperClass={cls.blockWrapper}
    >
      {MENU_ITEMS.map((item) => (
        <div
          key={item.key}
          className={cls.menuBlock}
          onClick={() => handleChangePage(item.path)}
        >
          <item.icon className={item.iconStyle} />
          <p>{item.title}</p>
        </div>
      ))}
    </ContentWindow>
  );
};
