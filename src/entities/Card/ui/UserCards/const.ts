import { ReactComponent as Baggage } from 'shared/assets/icons/baggage.svg';
import { ReactComponent as Biology } from 'shared/assets/icons/biology.svg';
import { ReactComponent as Facts } from 'shared/assets/icons/facts.svg';
import { ReactComponent as Health } from 'shared/assets/icons/health.svg';
import { ReactComponent as Hobby } from 'shared/assets/icons/hobby.svg';
import { ReactComponent as Profession } from 'shared/assets/icons/proffession.svg';

import type {
  ITypeIcon,
  IIconStyle,
  IIsOpenCards,
} from './ui/UserCards.interface';

import cls from './ui/UserCards.module.scss';

export const TYPES_ICONS: ITypeIcon = {
  BAGGAGE: Baggage,
  BIOLOGY: Biology,
  FACTS: Facts,
  HEALTH: Health,
  HOBBY: Hobby,
  PROFESSIONS: Profession,
};

export const ACTIVE_STYLES: IIconStyle = {
  BAGGAGE: cls.baggage,
  BIOLOGY: cls.biology,
  FACTS: cls.facts,
  HEALTH: cls.health,
  HOBBY: cls.hobby,
  PROFESSIONS: cls.profession,
};
