import { useLeaveGame } from 'entities/Game';
import { useTypedSelector } from 'shared/lib/hooks';

import { ReactComponent as Trash } from 'shared/assets/icons/trash.svg';
import { PlayerItem } from '../PlayerItem';

import { IS_ADMIN_STORAGE_NAME } from 'shared/consts/storageNames';

import cls from './PlayersInGameList.module.scss';

export const PlayersInGameList = () => {
  const { players, game } = useTypedSelector((state) => state.gameSlice);
  const isAdmin = localStorage.getItem(IS_ADMIN_STORAGE_NAME);
  const [deletePlayer] = useLeaveGame();

  const handleDeletePlayer = (playerId: string) => {
    deletePlayer({ id: playerId })
      .unwrap()
      .then(() => ({}))
      .catch(() => ({}));
  };

  if (!game.round)
    return (
      <div className={cls.list}>
        {players.map((player) => (
          <div key={player.id} className={cls.player}>
            <p>{player.number + 1}</p>
            <p>{player.name}</p>
            <p className={player.is_ready ? cls.ready : cls.notReady}>
              {player.is_ready ? 'Готов' : 'Не готов'}
            </p>
            {isAdmin ? (
              <button
                className={cls.button}
                onClick={() => handleDeletePlayer(player.id)}
              >
                <Trash />
              </button>
            ) : (
              <></>
            )}
          </div>
        ))}
      </div>
    );

  return (
    <div className={cls.list}>
      {players.map((player) => (
        <PlayerItem key={player.id} player={player} />
      ))}
    </div>
  );
};
