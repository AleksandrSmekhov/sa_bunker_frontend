import type { ISourceLinkValues } from './ui/ProjectInfo.interface';

export const SOURCE_LINKS_VALUES: ISourceLinkValues[] = [
  {
    title: 'Backend',
    href: 'https://gitlab.com/AleksandrSmekhov/sa_bunker_backend',
    key: 'backend-project-link',
  },
  {
    title: 'Frontend',
    href: 'https://gitlab.com/AleksandrSmekhov/sa_bunker_frontend',
    key: 'frontend-project-link',
  },
];
