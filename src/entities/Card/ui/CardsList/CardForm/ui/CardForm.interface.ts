import type {
  PlayerCardTypes,
  GameCardTypes,
} from 'entities/Card/model/consts/enums';

export interface IInputs {
  name: string;
  description: string;
}

export interface IFlags {
  isNameError: boolean;
  isDescriptionError: boolean;
}

export interface ICardFormProps {
  handleReset: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
  initialInputs: IInputs;
  type: PlayerCardTypes | GameCardTypes;
  cardId?: string;
}
