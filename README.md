# Frontend pet проекта - "Игра «Бункер»­"

## .env.example

- REACT_APP_API_BASE_URL - базовый URL для REST API
- REACT_APP_WS_BASE_URL - базовый URL для вебсокетов

## Стек

- TypeScript
- React
- React Router
- Redux
- Sass
- React-Select

## Установка

Установка зависимостей:

### `yarn` или `npm install`

Запуск режима отладки:

### `yarn start` или `npm run start`

Сборка production версии:

### `yarn build` или `npm run build`

## Страницы

- /menu - Меню игры
- /login - Страница авторизации
- /games - Страница игр
- /players - Страница пользователей
- /cards - Страница со списком карт
- /game/:gameId - Страница игры gameId
