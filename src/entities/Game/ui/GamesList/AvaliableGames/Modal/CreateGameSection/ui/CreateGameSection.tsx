import { useNavigate } from 'react-router-dom';

import { useCreateGame, useJoinGame } from 'entities/Game';

import { ReactComponent as Tick } from 'shared/assets/icons/tick.svg';

import type { ICreateGameSectionProps } from './CreateGameSection.interface';

import { RoutePath } from 'shared/consts/router';

import cls from './CreateGameSection.module.scss';

export const CreateGameSection = ({
  sectionStyle,
  name,
  password,
  setName,
  setPassword,
}: ICreateGameSectionProps) => {
  const [createGame] = useCreateGame();
  const [joinGame] = useJoinGame();
  const navigate = useNavigate();

  const handleEnterPassword = (event: React.ChangeEvent<HTMLInputElement>) => {
    setPassword(event.target.value);
  };

  const handleEnterName = (event: React.ChangeEvent<HTMLInputElement>) => {
    setName(event.target.value);
  };

  const handleCreateGame = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    createGame({ name, password })
      .unwrap()
      .then((result) =>
        joinGame({ game_id: result.id, password: result.password })
          .unwrap()
          .then(() => navigate(`${RoutePath.game}${result.id}`))
          .catch(() => ({}))
      )
      .catch(() => ({}));
  };

  return (
    <section className={sectionStyle}>
      <form className={cls.form} onSubmit={handleCreateGame}>
        <div className={cls.input}>
          <label htmlFor="newgameNameInput">Введите название:</label>
          <input
            id="newgameNameInput"
            autoComplete="off"
            value={name}
            onChange={handleEnterName}
            type="text"
            placeholder="Название"
          ></input>
          <label htmlFor="newGamePasswordInput">Введите пароль:</label>
          <input
            id="newGamePasswordInput"
            autoComplete="off"
            value={password}
            onChange={handleEnterPassword}
            type="password"
            placeholder="Пароль"
          ></input>
        </div>
        <button
          type="submit"
          disabled={!name}
          className={`${cls.button} ${name ? cls.active : cls.disabled}`}
        >
          Создать
          <Tick />
        </button>
      </form>
    </section>
  );
};
