export const BASE_WS_URL =
  process.env.REACT_APP_WS_BASE_URL || 'http://localhost:8000/api';
export const BASE_API_URL =
  process.env.REACT_APP_API_BASE_URL || 'ws://localhost:8000/ws';
