import type {
  CSSObjectWithLabel,
  OptionProps,
  GroupBase,
  SingleValueProps,
} from 'react-select';

import type { IOption } from '../SelectCardOrVote.interface';

// background: #2e3034;
//    color: ;

export const CONTROL_STYLES = (base: CSSObjectWithLabel) =>
  ({
    ...base,
    border: 'none',
    boxShadow: 'none',
    height: 40,
    minHeight: 40,
    borderRadius: 10,
    fontSize: 16,
    fontFamily: 'Shen',
    color: '#debe03',
    background: '#2e3034',
    '::before': {
      position: 'absolute',
      paddingLeft: 15,
      paddingTop: 0,
      top: 0,
      fontSize: 10,
      color: '#debe03',
    },
    position: 'relative',
  } as CSSObjectWithLabel);

export const SINGLE_VALUE_STYLES = (
  base: CSSObjectWithLabel,
  props: SingleValueProps<IOption, false, GroupBase<IOption>>
) =>
  ({
    ...base,
    margin: 0,
    lineHeight: '40px',
    color: props.data.color || '#debe03',
  } as CSSObjectWithLabel);

export const VALUE_CONTAINER_STYLES = (base: CSSObjectWithLabel) =>
  ({
    ...base,
    height: 40,
    paddingLeft: 15,
    paddingTop: 0,
    paddingBottom: 0,
    color: '#debe03',
  } as CSSObjectWithLabel);

export const PLACEHOLDER_STYLES = (base: CSSObjectWithLabel) =>
  ({
    ...base,
    fontWeight: 400,
    fontSize: 16,
    color: '#debe03',
  } as CSSObjectWithLabel);

export const DROPDOWN_INDICATOR_STYLES = (base: CSSObjectWithLabel) =>
  ({
    ...base,
    padding: 0,
    height: 40,
    width: 30,
    display: 'flex',
    svg: { margin: 'auto', fill: '#debe03' },
  } as CSSObjectWithLabel);

export const INDICATORS_CONTAINER_STYLES = (base: CSSObjectWithLabel) =>
  ({
    ...base,
    height: 40,
    svg: { cursor: 'pointer' },
  } as CSSObjectWithLabel);

export const MENU_STYLES = (base: CSSObjectWithLabel) =>
  ({
    ...base,
    margin: '5px 0px 0px 0px',
    borderRadius: 10,
    background: '#2e3034',
    zIndex: 3,
  } as CSSObjectWithLabel);

export const MENU_LIST_STYLES = (base: CSSObjectWithLabel) =>
  ({
    ...base,
    paddingTop: 0,
    maxHeight: 370,
    paddingBottom: 0,
    '::-webkit-scrollbar': {
      width: 5,
      height: 5,
    },
    '::-webkit-scrollbar-thumb': {
      boxShadow: 'inset 0 0 6px rgb(0 21 41)',
      borderRadius: 5,
    },
  } as CSSObjectWithLabel);

export const OPTION_STYLES = (
  base: CSSObjectWithLabel,
  props: OptionProps<IOption, false, GroupBase<IOption>>
) =>
  ({
    ...base,
    fontSize: 16,
    fontFamily: 'Shen',
    color: props.data.color || '#debe03',
    background: '#2e3034',
    ':first-of-type': { borderTopLeftRadius: 10, borderTopRightRadius: 10 },
    ':last-of-type': {
      borderBottomLeftRadius: 10,
      borderBottomRightRadius: 10,
    },
    ':hover': {
      cursor: 'pointer',
      background: '#f5d152',
    },
    ':active': { background: '#f5d152', color: '#2e3034' },
  } as CSSObjectWithLabel);
