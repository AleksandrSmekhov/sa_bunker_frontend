import type { PlayerCardTypes } from 'entities/Card/model/consts/enums';

export interface IOption {
  label: string;
  value: string;
  type: PlayerCardTypes;
}

export type IOptionColors = Record<PlayerCardTypes, string>;
