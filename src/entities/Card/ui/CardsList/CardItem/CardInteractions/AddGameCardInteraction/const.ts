import type { IOptionColors } from './ui/AddGameCardInteraction.interface';

export const OPTION_COLORS: IOptionColors = {
  BAGGAGE: '#293c92',
  BIOLOGY: '#e36d03',
  FACTS: '#04accd',
  HEALTH: '#cd2f2c',
  HOBBY: '#678e2d',
  PROFESSIONS: '#776f8f',
};
