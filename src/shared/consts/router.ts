export enum AppRoutes {
  LOGIN = 'login',
  CARDS = 'cards',
  MENU = 'menu',
  GAMES = 'games',
  GAME = 'game',
  PLAYERS = 'players',
  NOT_FOUND = 'not_found',
}

export const RoutePath: Record<AppRoutes, string> = {
  [AppRoutes.LOGIN]: '/login',
  [AppRoutes.NOT_FOUND]: '/not_found',
  [AppRoutes.CARDS]: '/cards',
  [AppRoutes.MENU]: '/menu',
  [AppRoutes.GAMES]: '/games',
  [AppRoutes.PLAYERS]: '/players',
  [AppRoutes.GAME]: '/game/',
};
