import type { IGetPlayerValues } from 'entities/Player/model/interfaces/IGetPlayers.interface';

export interface IPlayerItemProps {
  player: IGetPlayerValues;
  index: number;
  isAdmin: boolean;
}
