import { useState, useEffect } from 'react';

import { useGames } from 'shared/lib/hooks';

import { BASE_WS_URL } from 'shared/consts/urls';
import { USER_ID_STORAGE_NAME } from 'shared/consts/storageNames';
import { GET_GAMES_URL } from './consts/urls';

export const useGetGames = () => {
  const [isConnected, setIsConnected] = useState<boolean>(false);
  const [isError, setIsError] = useState<boolean>(false);
  const { setData } = useGames();
  const userId = localStorage.getItem(USER_ID_STORAGE_NAME);
  const socket = new WebSocket(`${BASE_WS_URL}${GET_GAMES_URL}?${userId}`);

  useEffect(() => {
    socket.onopen = () => {
      setIsConnected(true);
    };

    socket.onclose = () => {
      setIsConnected(false);
    };

    socket.onerror = () => {
      setIsError(true);
      setIsConnected(false);
    };

    socket.onmessage = (message) => {
      setData(JSON.parse(message.data));
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return { isConnected, isError, socket };
};
