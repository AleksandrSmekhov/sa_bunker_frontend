import { fetchBaseQuery, createApi } from '@reduxjs/toolkit/query/react';

import { USER_ID_STORAGE_NAME } from 'shared/consts/storageNames';

import { BASE_API_URL } from 'shared/consts/urls';

const baseQuery = fetchBaseQuery({
  baseUrl: BASE_API_URL,
  prepareHeaders: (headers) => {
    const userId = localStorage.getItem(USER_ID_STORAGE_NAME) || '';
    if (userId) {
      headers.set('Authorization', userId);
    }
    return headers;
  },
});

export const api = createApi({
  reducerPath: 'api',
  baseQuery,
  tagTypes: ['GameCards', 'PlayerCards'],
  endpoints: () => ({}),
});
