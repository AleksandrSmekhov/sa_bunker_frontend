import { useNavigate } from 'react-router-dom';

import { useTypedSelector } from 'shared/lib/hooks';
import { useLeaveGame } from 'entities/Game/model/GameApi';

import { ReactComponent as Exit } from 'shared/assets/icons/exit.svg';

import { getPlayersText } from 'shared/lib/funcs';

import { RoutePath } from 'shared/consts/router';

import cls from './GameInfo.module.scss';

export const GameInfo = () => {
  const { game, players, player } = useTypedSelector(
    (state) => state.gameSlice
  );
  const [leaveGame] = useLeaveGame();
  const navigate = useNavigate();

  const ready_players =
    players.reduce((result, player) => result + +player.is_ready, 0) +
    +player.is_ready;

  const handleExit = () => {
    leaveGame({ id: player.id })
      .unwrap()
      .then(() => navigate(RoutePath.games))
      .catch(() => ({}));
  };

  if (!game.round)
    return (
      <div className={`${cls.info} ${cls.waiting}`}>
        <p className={cls.title}>Ожидание игроков...</p>
        <div className={cls.infoLine}>
          <p className={cls.title}>Игроков:</p>
          <p>{players.length + 1} / 16</p>
        </div>
        <div className={cls.infoLine}>
          <p className={cls.title}>Готово:</p>
          <p>
            {ready_players} / {players.length + 1}{' '}
            {getPlayersText(ready_players)}
          </p>
        </div>
        <button className={cls.button} onClick={handleExit}>
          Выйти
          <Exit />
        </button>
      </div>
    );

  if (game.round > 6)
    return (
      <div className={cls.info}>
        <p className={cls.title}>Игра завершена</p>
        <div className={cls.infoLine}>
          <p className={cls.title}>Игроков:</p>
          <p>{players.length + 1}</p>
        </div>
      </div>
    );

  return (
    <div className={cls.info}>
      <div className={cls.infoLine}>
        <p className={cls.title}>Раунд: </p>
        <p>{game.round} / 6</p>
      </div>
      {game.is_voting ? (
        <p>Голосование...</p>
      ) : (
        <div className={cls.infoLine}>
          <p className={cls.title}>Ход:</p>
          <p>
            {game.current_player === player.number
              ? 'Ваш'
              : players.find((player) => player.number === game.current_player)
                  ?.name}
          </p>
        </div>
      )}
    </div>
  );
};
