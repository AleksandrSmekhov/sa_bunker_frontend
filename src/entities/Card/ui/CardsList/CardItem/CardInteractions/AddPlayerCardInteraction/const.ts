import type { IOptionColors } from './ui/AddPlayerCardInteraction.interface';

export const OPTION_COLORS: IOptionColors = {
  BUNKER: '#535457',
  DISASTER: '#ac8103',
  RISK: '#535457',
};
