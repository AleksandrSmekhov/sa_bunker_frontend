import { useState } from 'react';

import { ReactComponent as Arrow } from 'shared/assets/icons/arrow.svg';
import { ArchiveGameItem } from '../ArchiveGameItem';

import type { IArchiveGamesProps } from './ArchiveGames.interface';

import cls from './ArchiveGames.module.scss';

export const ArchiveGames = ({ games, styles }: IArchiveGamesProps) => {
  const [isOpen, setIsOpen] = useState<boolean>(false);

  const handleOpen = () => {
    setIsOpen((prev) => !prev);
  };

  return (
    <details open={isOpen} onToggle={handleOpen}>
      <summary className={cls.header}>
        <h2>Архив</h2>
        <Arrow className={`${cls.arrow} ${isOpen ? cls.rotateArrow : ''}`} />
      </summary>
      <div className={`${styles.block} ${cls.block}`}>
        {games.map((game) => (
          <ArchiveGameItem game={game} lineStyle={styles.game} key={game.id} />
        ))}
      </div>
    </details>
  );
};
