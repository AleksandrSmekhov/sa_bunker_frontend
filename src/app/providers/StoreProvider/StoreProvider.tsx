import { Provider } from 'react-redux';

import store from './config';

interface IStoreProviderProps {
  children: JSX.Element;
}

export const StoreProvider = ({ children }: IStoreProviderProps) => {
  return <Provider store={store}>{children}</Provider>;
};
