export enum GameCardTypes {
  risk = 'RISK',
  bunker = 'BUNKER',
  disaster = 'DISASTER',
}

export enum PlayerCardTypes {
  biology = 'BIOLOGY',
  health = 'HEALTH',
  hobby = 'HOBBY',
  baggage = 'BAGGAGE',
  facts = 'FACTS',
  professions = 'PROFESSIONS',
}

export enum InteractionTypes {
  positive = 'POSITIVE',
  negative = 'NEGATIVE',
}
