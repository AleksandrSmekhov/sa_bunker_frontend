import { useState } from 'react';

import { useTypedSelector } from 'shared/lib/hooks';
import { useReadyPlayer } from 'entities/Player';

import { ReactComponent as Arrow } from 'shared/assets/icons/arrow.svg';
import { UserCards } from 'entities/Card';
import { SelectCardOrVote } from 'entities/Game';

import cls from './UserInfo.module.scss';

export const UserInfo = () => {
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const { player, game } = useTypedSelector((state) => state.gameSlice);
  const [changeReady] = useReadyPlayer();

  const handleChangeReady = () => {
    changeReady({
      game_id: game.id,
      player_id: player.id,
      is_ready: !player.is_ready,
    })
      .unwrap()
      .then(() => ({}))
      .catch(() => ({}));
  };

  const handleOpen = () => {
    setIsOpen((prev) => !prev);
  };

  if (!game.round)
    return (
      <div className={`${cls.info} ${cls.wait}`}>
        <div className={cls.infoLine}>
          <p className={cls.title}>Номер:</p>
          <p>{player.number + 1}</p>
        </div>
        <div className={cls.infoLine}>
          <p className={cls.title}>Готовность:</p>
          <p className={player.is_ready ? cls.green : cls.red}>
            {player.is_ready ? 'Готов' : 'Не готов'}
          </p>
        </div>
        <button className={cls.button} onClick={handleChangeReady}>
          {player.is_ready ? 'Не готов' : 'Готов'}
        </button>
      </div>
    );

  return (
    <details open={isOpen} onToggle={handleOpen} className={cls.details}>
      <summary className={`${cls.info} ${cls.game} `}>
        <Arrow className={`${cls.arrow} ${isOpen ? cls.rotateArrow : ''}`} />
        {game.round > 6 ? (
          <p className={player.is_in_game ? cls.green : cls.red}>
            {player.is_in_game ? 'Вы попали в бункер' : 'Вы изгнаны'}
          </p>
        ) : (
          <>
            <p>Ваши карты</p>
            <p className={player.is_in_game ? cls.green : cls.red}>
              {player.is_in_game ? 'Вы живы' : 'Вы изгнаны'}
            </p>
          </>
        )}
      </summary>
      <UserCards />
      <SelectCardOrVote />
    </details>
  );
};
