import type {
  IGameCards,
  IPlayerCards,
} from 'entities/Card/model/interfaces/IGetCards.interface';
import {
  GameCardTypes,
  PlayerCardTypes,
} from 'entities/Card/model/consts/enums';

interface ICardsValues {
  key: string;
  title: string;
}

export interface IGameCardsValues extends ICardsValues {
  objectKey: keyof IGameCards;
  type: GameCardTypes;
}

export interface IPlayerCardsValues extends ICardsValues {
  objectKey: keyof IPlayerCards;
  type: PlayerCardTypes;
}

export type ICardsTypeValues = Record<GameCardTypes | PlayerCardTypes, string>;
