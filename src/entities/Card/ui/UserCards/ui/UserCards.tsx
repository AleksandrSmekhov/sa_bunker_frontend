import { useTypedSelector } from 'shared/lib/hooks';

import { ReactComponent as Plus } from 'shared/assets/icons/plus.svg';
import { ReactComponent as Minus } from 'shared/assets/icons/minus.svg';

import type { PlayerCardTypes } from 'entities/Card/model/consts/enums';

import { InteractionTypes } from 'entities/Card/model/consts/enums';
import { ACTIVE_STYLES, TYPES_ICONS } from '../const';

import cls from './UserCards.module.scss';

export const UserCards = () => {
  const { player } = useTypedSelector((state) => state.gameSlice);

  const handlePreventPropagination = (
    event: React.SyntheticEvent<HTMLDetailsElement, Event>
  ) => {
    event.stopPropagation();
  };

  const getIcon = (type: PlayerCardTypes) => {
    const Icon = TYPES_ICONS[type];
    return <Icon />;
  };

  return (
    <div className={cls.cards}>
      {player.cards.map((card) => (
        <details key={card.id} onToggle={handlePreventPropagination}>
          <summary className={`${cls.card} ${ACTIVE_STYLES[card.type]}`}>
            {getIcon(card.type)}
            <p>{card.name}</p>
            <p className={card.is_open ? cls.green : cls.red}>
              {card.is_open ? 'Карта открыта' : 'Карта закрыта'}
            </p>
          </summary>
          <div className={cls.cardDescription}>
            {card.description.split('\n').map((line, index) => (
              <p key={`${card.id}-${index}`}>{line}</p>
            ))}
            <div className={cls.interactions}>
              {card.interactions
                .filter((interaction) => interaction.game_card)
                .map((interaction, index) => (
                  <div
                    key={`yout-interaction-${index}`}
                    className={`${
                      interaction.type === InteractionTypes.positive
                        ? cls.positive
                        : cls.negative
                    } ${cls.interactionLine}`}
                  >
                    {interaction.type === InteractionTypes.positive ? (
                      <Plus />
                    ) : (
                      <Minus />
                    )}
                    <p>{interaction.game_card}</p>
                  </div>
                ))}
            </div>
          </div>
        </details>
      ))}
    </div>
  );
};
