import { createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';

import type { IGetCardsResult } from 'entities/Card/model/interfaces/IGetCards.interface';

import { CARDS_SLICE_NAME, INITIAL_VALUES } from './const';

export const cardsSlice = createSlice({
  name: CARDS_SLICE_NAME,
  initialState: INITIAL_VALUES,
  reducers: {
    setData(_, { payload }: PayloadAction<IGetCardsResult>) {
      return payload;
    },
  },
});

export const cardsSliceActions = cardsSlice.actions;
