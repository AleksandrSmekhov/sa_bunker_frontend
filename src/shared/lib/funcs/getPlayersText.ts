const A_ENDING_NUMBERS = [2, 3, 4];

export const getPlayersText = (number: number) => {
  if (number === 1 || (number > 20 && number % 10 === 1)) return 'игрок';
  if (
    A_ENDING_NUMBERS.includes(number) ||
    (number > 20 && A_ENDING_NUMBERS.includes(number % 10))
  )
    return 'игрока';
  return 'игроков';
};
