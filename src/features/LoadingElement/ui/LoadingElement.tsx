import { ReactComponent as Loading } from 'shared/assets/icons/loading.svg';

import cls from './LoadingElement.module.scss';

interface ILoadingElementProps {
  extraClass?: string;
}

export const LoadingElement = ({ extraClass }: ILoadingElementProps) => {
  return <Loading className={`${cls.loading} ${extraClass || ''}`} />;
};
