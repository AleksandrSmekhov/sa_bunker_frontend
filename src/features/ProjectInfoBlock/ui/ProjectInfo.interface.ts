export interface ISourceLinkValues {
  title: string;
  href: string;
  key: string;
}
