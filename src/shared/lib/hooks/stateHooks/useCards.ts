import { useMemo } from 'react';
import { useDispatch } from 'react-redux';
import { bindActionCreators } from '@reduxjs/toolkit';

import { cardsSliceActions } from 'shared/slices';

export const useCards = () => {
  const dispatch = useDispatch();

  return useMemo(
    () => bindActionCreators(cardsSliceActions, dispatch),
    [dispatch]
  );
};
