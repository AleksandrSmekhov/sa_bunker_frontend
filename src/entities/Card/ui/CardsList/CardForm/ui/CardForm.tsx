import { useState } from 'react';

import {
  useCreateGameCard,
  useCreatePlayerCard,
  useUpdatePlayerCard,
  useUpdateGameCard,
  useDeleteGameCard,
  useDeletePlayerCard,
} from 'entities/Card';

import { ReactComponent as Tick } from 'shared/assets/icons/tick.svg';
import { ReactComponent as Cross } from 'shared/assets/icons/cross.svg';
import { ReactComponent as Trash } from 'shared/assets/icons/trash.svg';

import type { ICardFormProps, IInputs, IFlags } from './CardForm.interface';

import {
  GameCardTypes,
  PlayerCardTypes,
} from 'entities/Card/model/consts/enums';
import { INITIAL_FLAGS } from '../const';

import cls from './CardForm.module.scss';

export const CardForm = ({
  handleReset,
  initialInputs,
  type,
  cardId,
}: ICardFormProps) => {
  const [inputsValues, setInputsValues] = useState<IInputs>(initialInputs);
  const [flags, setFlags] = useState<IFlags>(INITIAL_FLAGS);

  const [createGameCard] = useCreateGameCard();
  const [createPlayerCard] = useCreatePlayerCard();
  const [updateGameCard] = useUpdateGameCard();
  const [updatePlayerCard] = useUpdatePlayerCard();
  const [deleteGameCard] = useDeleteGameCard();
  const [deletePlayerCard] = useDeletePlayerCard();

  const handleChangeName = (event: React.ChangeEvent<HTMLInputElement>) => {
    !flags.isNameError || setFlags((prev) => ({ ...prev, isNameError: false }));
    setInputsValues((prev) => ({ ...prev, name: event.target.value }));
  };

  const handleUndo = (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    setInputsValues(initialInputs);
    handleReset(event);
  };

  const handleChangeDescription = (
    event: React.ChangeEvent<HTMLTextAreaElement>
  ) => {
    !flags.isDescriptionError ||
      setFlags((prev) => ({ ...prev, isDescriptionError: false }));
    setInputsValues((prev) => ({ ...prev, description: event.target.value }));
  };

  const handleSubmit = (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    event.preventDefault();

    const { name, description } = inputsValues;

    if (!name || !description) {
      name || setFlags((prev) => ({ ...prev, isNameError: true }));
      description ||
        setFlags((prev) => ({ ...prev, isDescriptionError: true }));
      return;
    }

    if (!cardId) {
      Object.values(GameCardTypes).includes(type as GameCardTypes)
        ? createGameCard({ name, description, type: type as GameCardTypes })
            .unwrap()
            .then(() => setInputsValues(initialInputs))
            .catch(() =>
              setFlags({
                isNameError: true,
                isDescriptionError: true,
              })
            )
        : createPlayerCard({ name, description, type: type as PlayerCardTypes })
            .unwrap()
            .then(() => setInputsValues(initialInputs))
            .catch(() =>
              setFlags({
                isNameError: true,
                isDescriptionError: true,
              })
            );
      return;
    }

    Object.values(GameCardTypes).includes(type as GameCardTypes)
      ? updateGameCard({
          name,
          description,
          type: type as GameCardTypes,
          id: cardId,
        })
          .unwrap()
          .then(() => handleReset(event))
          .catch(() =>
            setFlags({
              isNameError: true,
              isDescriptionError: true,
            })
          )
      : updatePlayerCard({
          name,
          description,
          type: type as PlayerCardTypes,
          id: cardId,
        })
          .then(() => handleReset(event))
          .catch(() =>
            setFlags({
              isNameError: true,
              isDescriptionError: true,
            })
          );
  };

  const handleDelete = (id: string) => {
    Object.values(GameCardTypes).includes(type as GameCardTypes)
      ? deleteGameCard({ id })
      : deletePlayerCard({ id });
  };

  return (
    <form className={cls.card}>
      <input
        type="text"
        id="newCard-name"
        autoComplete="off"
        placeholder="Название"
        value={inputsValues.name}
        onChange={handleChangeName}
        className={`${cls.input} ${flags.isNameError ? cls.error : ''}`}
      ></input>
      <textarea
        id="newCard-description"
        autoComplete="off"
        placeholder="Введите описание..."
        rows={12}
        value={inputsValues.description}
        onChange={handleChangeDescription}
        className={`${cls.textarea} ${
          flags.isDescriptionError ? cls.error : ''
        }`}
      ></textarea>
      <div
        className={`${cls.buttons} ${
          cardId ? cls.editButtons : cls.createButtons
        }`}
      >
        <button type="submit" onClick={handleSubmit}>
          <Tick className={cls.tick} />
        </button>
        {cardId ? (
          <button onClick={() => handleDelete(cardId)} type="button">
            <Trash className={cls.trash} />
          </button>
        ) : (
          <></>
        )}
        <button type="reset" onClick={handleUndo}>
          <Cross className={cls.cross} />
        </button>
      </div>
    </form>
  );
};
