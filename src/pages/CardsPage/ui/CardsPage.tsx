import { useGetCards, CardsList } from 'entities/Card';

import { ReactComponent as Skull } from 'shared/assets/icons/skull.svg';
import { Window } from 'features/Window';
import { LoadingElement } from 'features/LoadingElement';

import cls from './CardsPage.module.scss';

export const CardsPage = () => {
  const { isConnected, isError, socket } = useGetCards();

  const handleDisconnect = () => {
    socket.close();
  };

  if (!isConnected && !isError)
    return (
      <Window header="Карты">
        <LoadingElement />
      </Window>
    );

  if (isError)
    return (
      <Window header="Карты">
        <div className={cls.errorMessage}>
          <h1>Ошибка подключения</h1>
          <Skull />
        </div>
      </Window>
    );

  return (
    <Window header="Карты" extraExitFunc={handleDisconnect}>
      <CardsList />
    </Window>
  );
};
