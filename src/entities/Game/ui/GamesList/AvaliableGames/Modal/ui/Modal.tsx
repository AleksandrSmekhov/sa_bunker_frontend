import { useState } from 'react';

import { ReactComponent as Cross } from 'shared/assets/icons/cross.svg';
import { EnterPasswordSection } from '../EnterPasswordSection';
import { CreateGameSection } from '../CreateGameSection';

import type { IModalProps } from './Modal.interface';

import cls from './Modal.module.scss';

export const Modal = ({ modalRef, setGameId, gameId }: IModalProps) => {
  const [gameName, setGameName] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const [isError, setIsError] = useState<boolean>(false);

  const handleClose = () => {
    modalRef.current?.close();
    setGameId('');
    setGameName('');
    setPassword('');
    setIsError(false);
  };

  const handlePreventClosing = (
    event: React.MouseEvent<HTMLDivElement, MouseEvent>
  ) => {
    event.stopPropagation();
  };

  return (
    <dialog onClick={handleClose} ref={modalRef} className={cls.modal}>
      <div onClick={handlePreventClosing} className={cls.container}>
        <header className={cls.header}>
          <h3>{gameId ? 'Вход в игру' : 'Создание игры'}</h3>
          <Cross onClick={handleClose} />
        </header>
        {gameId ? (
          <EnterPasswordSection
            sectionStyle={cls.content}
            gameId={gameId}
            isError={isError}
            password={password}
            setIsError={setIsError}
            setPassword={setPassword}
          />
        ) : (
          <CreateGameSection
            sectionStyle={cls.content}
            name={gameName}
            password={password}
            setName={setGameName}
            setPassword={setPassword}
          />
        )}
      </div>
    </dialog>
  );
};
