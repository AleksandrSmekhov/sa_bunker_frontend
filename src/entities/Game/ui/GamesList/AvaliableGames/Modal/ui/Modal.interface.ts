export interface IModalProps {
  modalRef: React.RefObject<HTMLDialogElement>;
  setGameId: React.Dispatch<React.SetStateAction<string>>;
  gameId: string;
}
