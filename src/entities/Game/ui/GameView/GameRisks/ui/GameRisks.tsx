import { useTypedSelector } from 'shared/lib/hooks';

import { ReactComponent as Bunker } from 'shared/assets/icons/bunker.svg';
import { ReactComponent as Risk } from 'shared/assets/icons/risk.svg';
import { ReactComponent as Disaster } from 'shared/assets/icons/disaster.svg';
import { CardInfo } from 'entities/Card';

import { GameCardTypes } from 'entities/Card/model/consts/enums';

import cls from './GameRisks.module.scss';

export const GameRisks = () => {
  const { game, risks } = useTypedSelector((state) => state.gameSlice);

  return (
    <div className={cls.info}>
      <h3>Катастрофа</h3>
      <div>
        <details>
          <summary className={cls.disaster}>
            <Disaster className={cls.active} />
            <p>{game.disaster?.name}</p>
            <div className={cls.icons}>
              {risks.map((risk) =>
                risk.type === GameCardTypes.bunker ? (
                  <Bunker
                    key={`icon-${risk.id}`}
                    className={risk.is_open ? cls.active : cls.inactive}
                  />
                ) : (
                  <Risk
                    key={`icon-${risk.id}`}
                    className={risk.is_open ? cls.active : cls.inactive}
                  />
                )
              )}
            </div>
          </summary>
          <div className={cls.description}>
            {game.disaster?.description.split('\n').map((line, index) => (
              <p key={`description-line-${index}`}>{line}</p>
            ))}
          </div>
        </details>
        <div className={cls.risks}>
          {risks.map((risk) => (
            <CardInfo
              key={risk.id}
              description={risk.description}
              isOpen={risk.is_open}
              name={risk.name}
              type={risk.type}
            />
          ))}
        </div>
      </div>
      <h3>Игроки</h3>
    </div>
  );
};
