const GAMES_URL = '/games/';

export const GET_GAMES_URL = GAMES_URL;

const GAME_URL = '/game/';

export const GET_GAME_URL = GAME_URL;
export const CREATE_GAME_URL = GAME_URL;
export const DELETE_GAME_URL = GAME_URL;

const GAME_PLAYER_URL = '/game_player/';

export const JOIN_GAME_URL = GAME_PLAYER_URL;
export const LEAVE_GAME_URL = GAME_PLAYER_URL;
