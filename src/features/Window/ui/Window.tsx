import { useNavigate } from 'react-router-dom';

import type { IWindowProps } from './Window.interface';

import { RoutePath } from 'shared/consts/router';

import cls from './Window.module.scss';

export const Window = ({
  header,
  children,
  extraExitFunc,
  extraWindowCls,
}: IWindowProps) => {
  const navigate = useNavigate();

  const handleBack = () => {
    if (extraExitFunc) extraExitFunc();
    navigate(RoutePath.menu);
  };

  return (
    <div className={cls.windowWrapper}>
      <div className={cls.headerBlock}>
        <button onClick={handleBack} className={cls.backButton}>
          ← Назад
        </button>
        <h1 className={cls.header}>{header}</h1>
      </div>
      <div className={`${cls.window} ${extraWindowCls || ''}`}>{children}</div>
    </div>
  );
};
