import { api } from 'shared/api';

import type { ILoginBody, ILoginResult } from './interfaces/ILogin.interface';

import { LOGIN_URL } from './consts/urls';
import { POST_METHOD } from 'shared/consts/requestsMethods';

const LoginApi = api.injectEndpoints({
  endpoints: (build) => ({
    login: build.mutation<ILoginResult, ILoginBody>({
      query: (body) => ({
        url: LOGIN_URL,
        method: POST_METHOD,
        body,
      }),
    }),
  }),
});

export const useLogin = LoginApi.useLoginMutation;
