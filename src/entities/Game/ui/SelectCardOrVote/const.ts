import type { ITypeColor } from './ui/SelectCardOrVote.interface';

export const TYPES_COLORS: ITypeColor = {
  BAGGAGE: '#324bb9',
  BIOLOGY: '#e36d03',
  FACTS: '#04accd',
  HEALTH: '#cd2f2c',
  HOBBY: '#678e2d',
  PROFESSIONS: '#776f8f',
};
