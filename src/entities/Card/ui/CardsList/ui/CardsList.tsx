import { useTypedSelector } from 'shared/lib/hooks';

import { CardItem } from '../CardItem';
import { AddCardItem } from '../AddCardItem';

import { IS_ADMIN_STORAGE_NAME } from 'shared/consts/storageNames';
import {
  PLAYER_CARDS_VALUES,
  GAME_CARDS_VALUES,
  CARDS_TYPE_COLOR_VALUES,
} from '../const';

import cls from './CardsList.module.scss';

export const CardsList = () => {
  const { game_cards, player_cards } = useTypedSelector(
    (state) => state.cardsSlice
  );
  const isAdmin = !!localStorage.getItem(IS_ADMIN_STORAGE_NAME);

  return (
    <div className={cls.cards}>
      {GAME_CARDS_VALUES.map((values) => (
        <div key={values.key} className={cls.cardsSection}>
          <h3 className={cls.sectionTitle}>{values.title}</h3>
          <div className={cls.cardsList}>
            {game_cards[values.objectKey].map((card) => (
              <CardItem
                card={card}
                key={card.id}
                extraColor={CARDS_TYPE_COLOR_VALUES[card.type]}
                isAdmin={isAdmin}
              />
            ))}
            {isAdmin ? (
              <AddCardItem
                extraColor={CARDS_TYPE_COLOR_VALUES[values.type]}
                type={values.type}
              />
            ) : (
              <></>
            )}
          </div>
        </div>
      ))}
      {PLAYER_CARDS_VALUES.map((values) => (
        <div key={values.key} className={cls.cardsSection}>
          <h3 className={cls.sectionTitle}>{values.title}</h3>
          <div className={cls.cardsList}>
            {player_cards[values.objectKey].map((card) => (
              <CardItem
                card={card}
                key={card.id}
                extraColor={CARDS_TYPE_COLOR_VALUES[card.type]}
                isAdmin={isAdmin}
              />
            ))}
            {isAdmin ? (
              <AddCardItem
                extraColor={CARDS_TYPE_COLOR_VALUES[values.type]}
                type={values.type}
              />
            ) : (
              <></>
            )}
          </div>
        </div>
      ))}
    </div>
  );
};
