import { useMemo } from 'react';
import { useDispatch } from 'react-redux';
import { bindActionCreators } from '@reduxjs/toolkit';

import { gameSliceActions } from 'shared/slices';

export const useGame = () => {
  const dispatch = useDispatch();

  return useMemo(
    () => bindActionCreators(gameSliceActions, dispatch),
    [dispatch]
  );
};
