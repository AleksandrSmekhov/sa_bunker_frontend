export interface IVotePlayerBody {
  player_id: string;
  game_id: string;
  vote: string;
}
