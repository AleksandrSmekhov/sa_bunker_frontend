import { ICardsTypeValues, IFlags } from './ui/CardItem.interface';

export const CARDS_TYPE_TRANSLATION_VALUES: ICardsTypeValues = {
  BAGGAGE: 'Багаж',
  BIOLOGY: 'Биология',
  BUNKER: 'Бункер',
  DISASTER: 'Катастрофа',
  FACTS: 'Факт',
  HEALTH: 'Здоровье',
  HOBBY: 'Хобби',
  PROFESSIONS: 'Профессия',
  RISK: 'Угроза',
};

export const INITIAL_FLAGS: IFlags = {
  isInEdit: false,
  isOpen: false,
};
