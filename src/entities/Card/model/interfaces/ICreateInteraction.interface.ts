import type { InteractionTypes } from '../consts/enums';

export interface ICreateInteractionBody {
  player_card: string;
  game_card: string;
  type: InteractionTypes;
}
