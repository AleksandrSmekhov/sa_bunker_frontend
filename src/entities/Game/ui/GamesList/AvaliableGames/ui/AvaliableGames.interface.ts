import type { IAvaliableGame } from 'entities/Game/model/interfaces/IGetGames.interface';

export interface IAvaliableGamesProps {
  games: IAvaliableGame[];
  isAdmin: boolean;
  styles: Record<string, string>;
}
