import type { CSSObjectWithLabel } from 'react-select';

export const CONTROL_STYLES = (
  base: CSSObjectWithLabel,
  backgroundColor: string | undefined
) =>
  ({
    ...base,
    border: 'none',
    boxShadow: 'none',
    height: 40,
    minHeight: 40,
    borderRadius: 10,
    fontSize: 16,
    fontFamily: 'Shen',
    color: '#f5f6f6',
    background: backgroundColor || '#caac44',
    '::before': {
      position: 'absolute',
      paddingLeft: 15,
      paddingTop: 0,
      top: 0,
      fontSize: 10,
      color: '#8e929b',
    },
    position: 'relative',
  } as CSSObjectWithLabel);

export const SINGLE_VALUE_STYLES = (base: CSSObjectWithLabel) =>
  ({
    ...base,
    margin: 0,
    lineHeight: '40px',
    color: '#f5f6f6',
  } as CSSObjectWithLabel);

export const VALUE_CONTAINER_STYLES = (base: CSSObjectWithLabel) =>
  ({
    ...base,
    height: 40,
    paddingLeft: 15,
    paddingTop: 0,
    paddingBottom: 0,
    color: '#f5f6f6',
  } as CSSObjectWithLabel);

export const PLACEHOLDER_STYLES = (base: CSSObjectWithLabel) =>
  ({
    ...base,
    fontWeight: 400,
    fontSize: 16,
    color: '#f5f6f6',
  } as CSSObjectWithLabel);

export const DROPDOWN_INDICATOR_STYLES = (base: CSSObjectWithLabel) =>
  ({
    ...base,
    padding: 0,
    height: 40,
    width: 30,
    display: 'flex',
    svg: { margin: 'auto', fill: '#f5f6f6' },
  } as CSSObjectWithLabel);

export const INDICATORS_CONTAINER_STYLES = (base: CSSObjectWithLabel) =>
  ({
    ...base,
    height: 40,
    svg: { cursor: 'pointer' },
  } as CSSObjectWithLabel);

export const MENU_STYLES = (base: CSSObjectWithLabel) =>
  ({
    ...base,
    margin: '5px 0px 0px 0px',
    borderRadius: 10,
    background: '#caac44',
    zIndex: 3,
  } as CSSObjectWithLabel);

export const MENU_LIST_STYLES = (base: CSSObjectWithLabel) =>
  ({
    ...base,
    paddingTop: 0,
    maxHeight: 370,
    paddingBottom: 0,
    '::-webkit-scrollbar': {
      width: 5,
      height: 5,
    },
    '::-webkit-scrollbar-thumb': {
      boxShadow: 'inset 0 0 6px rgb(0 21 41)',
      borderRadius: 5,
    },
  } as CSSObjectWithLabel);

export const OPTION_STYLES = (
  base: CSSObjectWithLabel,
  backgroundColor: string
) =>
  ({
    ...base,
    fontSize: 16,
    backgroundColor: backgroundColor,
    fontFamily: 'Shen',
    color: '#f5f6f6',
    ':first-of-type': { borderTopLeftRadius: 10, borderTopRightRadius: 10 },
    ':last-of-type': {
      borderBottomLeftRadius: 10,
      borderBottomRightRadius: 10,
    },
    ':hover': {
      cursor: 'pointer',
    },
    ':active': { background: backgroundColor },
  } as CSSObjectWithLabel);

export const INPUT_STYLES = (base: CSSObjectWithLabel) =>
  ({ ...base, color: '#f5f6f6' } as CSSObjectWithLabel);
