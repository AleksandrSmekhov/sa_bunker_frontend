import { useGetPlayers } from 'entities/Player';

import { ReactComponent as Skull } from 'shared/assets/icons/skull.svg';
import { Window } from 'features/Window';
import { LoadingElement } from 'features/LoadingElement';
import { PlayersList } from 'entities/Player';

import cls from './PlayersPage.module.scss';

export const PlayersPage = () => {
  const { isConnected, isError, socket } = useGetPlayers();

  const handleDisconnect = () => {
    socket.close();
  };

  if (!isConnected && !isError)
    return (
      <Window header="Игроки">
        <LoadingElement />
      </Window>
    );

  if (isError)
    return (
      <Window header="Игроки">
        <div className={cls.errorMessage}>
          <h1>Ошибка подключения</h1>
          <Skull />
        </div>
      </Window>
    );

  return (
    <Window header="Игроки" extraExitFunc={handleDisconnect}>
      <PlayersList />
    </Window>
  );
};
