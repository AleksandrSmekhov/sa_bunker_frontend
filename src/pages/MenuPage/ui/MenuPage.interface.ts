export interface IMenuItem {
  key: string;
  title: string;
  path: string;
  icon: React.FC<React.SVGProps<SVGSVGElement>>;
  iconStyle: string;
}
