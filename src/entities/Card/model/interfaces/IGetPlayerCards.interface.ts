import { PlayerCardTypes } from '../consts/enums';

export interface IGetPlayerCardsQuery {
  game_card_id: string;
}

export interface IGetPlayerCardsResult {
  id: string;
  name: string;
  description: string;
  type: PlayerCardTypes;
}
