import { useNavigate } from 'react-router-dom';

import { useJoinGame } from 'entities/Game/model/GameApi';

import { ReactComponent as Enter } from 'shared/assets/icons/exit.svg';

import type { IEnterPasswordSectionProps } from './EnterPasswordSection.interface';

import { RoutePath } from 'shared/consts/router';

import cls from './EnterPasswordSection.module.scss';

export const EnterPasswordSection = ({
  sectionStyle,
  gameId,
  isError,
  password,
  setIsError,
  setPassword,
}: IEnterPasswordSectionProps) => {
  const navigate = useNavigate();
  const [joinGame] = useJoinGame();

  const handleEnterPassword = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (isError && event.target.value) setIsError(false);
    setPassword(event.target.value);
  };

  const handleEnter = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    joinGame({ game_id: gameId, password: password })
      .unwrap()
      .then(() => navigate(`${RoutePath.game}${gameId}`))
      .catch(() => setIsError(true));
  };

  return (
    <section className={sectionStyle}>
      <form className={cls.form} onSubmit={handleEnter}>
        <div className={cls.input}>
          <label htmlFor="gamePasswordInput">Введите пароль:</label>
          <input
            id="gamePasswordInput"
            autoComplete="off"
            value={password}
            onChange={handleEnterPassword}
            type="password"
            placeholder="Пароль"
            className={isError ? cls.error : ''}
          ></input>
        </div>
        {isError ? <p className={cls.wrongPassword}>Неверные пароль</p> : <></>}
        <button
          type="submit"
          disabled={!password}
          className={`${cls.button} ${password ? cls.active : cls.disabled}`}
        >
          Войти
          <Enter />
        </button>
      </form>
    </section>
  );
};
