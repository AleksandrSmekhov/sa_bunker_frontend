import type { IGetGameResult } from 'entities/Game/model/interfaces/IGetGame.interface';

export const INITIAL_VALUES: IGetGameResult = {
  game: { current_player: 0, id: '', is_voting: false, name: '', round: 0 },
  player: {
    cards: [],
    id: '',
    is_in_game: true,
    is_ready: false,
    name: '',
    number: 0,
    vote: '',
  },
  players: [],
  risks: [],
};

export const GAME_SLICE_NAME = 'gameSlice';
