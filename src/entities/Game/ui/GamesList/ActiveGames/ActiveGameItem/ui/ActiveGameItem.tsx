import { useNavigate } from 'react-router-dom';

import { ReactComponent as Enter } from 'shared/assets/icons/exit.svg';

import { getPlayersText } from 'shared/lib/funcs';

import type { IActiveGameItemProps } from './ActiveGameItem.interface';

import { RoutePath } from 'shared/consts/router';

import cls from './ActiveGameItem.module.scss';

export const ActiveGameItem = ({ game, lineStyle }: IActiveGameItemProps) => {
  const navigate = useNavigate();

  const handleEnterGame = () => {
    navigate(`${RoutePath.game}${game.id}`);
  };

  if (!game.round)
    return (
      <div className={lineStyle}>
        <p>{game.name}</p>
        <p className={cls.statusText}>Ожидание игроков...</p>
        <p>
          {game.players} {getPlayersText(game.players)}
        </p>

        <button className={cls.enterButton} onClick={handleEnterGame}>
          <Enter />
        </button>
      </div>
    );

  return (
    <div className={lineStyle}>
      <p>{game.name}</p>
      <p className={cls.disaterText}>{game.disaster}</p>
      <p className={cls.statusText}>
        {game.is_voting
          ? 'Голосование...'
          : `Ход игрока ${game.current_player + 1} / ${game.players}`}
      </p>
      <p>Раунд: {game.round} / 6</p>
      <button className={cls.enterButton} onClick={handleEnterGame}>
        <Enter />
      </button>
    </div>
  );
};
