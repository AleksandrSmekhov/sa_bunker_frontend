import { useState } from 'react';
import { useNavigate } from 'react-router-dom';

import { useLogin } from '../model/LoginApi';

import { ContentWindow } from 'features/ContentWindow';
import { ProjectInfoBlock } from 'features/ProjectInfoBlock';

import { capitalizeString } from 'shared/lib/funcs';

import { RoutePath } from 'shared/consts/router';
import {
  USER_ID_STORAGE_NAME,
  IS_ADMIN_STORAGE_NAME,
  USER_NAME_STORAGE_NAME,
} from 'shared/consts/storageNames';

import cls from './LoginPage.module.scss';

export const LoginPage = () => {
  const [inputValue, setInputValue] = useState<string>('');
  const [isError, setIsError] = useState<boolean>(false);
  const [login] = useLogin();
  const navigate = useNavigate();

  const handleInput = (event: React.ChangeEvent<HTMLInputElement>) => {
    setInputValue(event.target.value);

    if (isError) {
      setIsError(false);
    }
  };

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    if (!inputValue) {
      setIsError(true);
      return;
    }

    login({ login: capitalizeString(inputValue) })
      .unwrap()
      .then((result) => {
        localStorage.setItem(USER_ID_STORAGE_NAME, result.id);
        localStorage.setItem(USER_NAME_STORAGE_NAME, result.name);
        localStorage.setItem(
          IS_ADMIN_STORAGE_NAME,
          result.is_admin ? 'admin' : ''
        );
        navigate(RoutePath.menu);
      })
      .catch(() => setIsError(true));
  };

  return (
    <ContentWindow header="Вход">
      <form className={cls.form} onSubmit={handleSubmit}>
        <div className={cls.inputBlock}>
          <p className={cls.inputHint}>Введите имя:</p>
          <input
            id="user-name-field"
            autoComplete="off"
            className={`${cls.input} ${isError ? cls.errorBorder : ''}`}
            value={inputValue}
            onChange={handleInput}
          />
        </div>
        <button type="submit" className={cls.button}>
          Войти
        </button>
      </form>
      <ProjectInfoBlock />
    </ContentWindow>
  );
};
