import { useParams } from 'react-router-dom';

import { useTypedSelector } from 'shared/lib/hooks';
import { useGetGame } from 'entities/Game';

import { Window } from 'features/Window';
import { LoadingElement } from 'features/LoadingElement';
import { ReactComponent as Skull } from 'shared/assets/icons/skull.svg';
import { GameView } from 'entities/Game';

import cls from './GamePage.module.scss';

export const GamePage = () => {
  const { id } = useParams();
  const { isConnected, isError, socket } = useGetGame(id || '');
  const { game } = useTypedSelector((state) => state.gameSlice);

  const handleDisconnect = () => {
    socket.close();
  };

  if (!isConnected && !isError)
    return (
      <Window header="Игра">
        <LoadingElement />
      </Window>
    );

  if (isError)
    return (
      <Window header="Игра">
        <div className={cls.errorMessage}>
          <h1>Ошибка подключения</h1>
          <Skull />
        </div>
      </Window>
    );

  return (
    <Window
      header={`Игра - ${game.name}`}
      extraExitFunc={handleDisconnect}
      extraWindowCls={cls.noWindowPadding}
    >
      <GameView />
    </Window>
  );
};
