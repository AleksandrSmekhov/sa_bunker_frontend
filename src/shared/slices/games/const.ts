import type { IGetGamesResult } from 'entities/Game/model/interfaces/IGetGames.interface';

export const INITIAL_VALUES: IGetGamesResult = {
  active: [],
  archive: [],
  avaliable: [],
};

export const GAMES_SLICE_NAME = 'gamesSlice';
