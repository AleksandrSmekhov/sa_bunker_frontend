import {
  PlayerCardTypes,
  GameCardTypes,
} from 'entities/Card/model/consts/enums';

export interface IAddCardItemProps {
  extraColor: string;
  type: PlayerCardTypes | GameCardTypes;
}

export interface IFlags {
  isNameError: boolean;
  isDescriptionError: boolean;
  isOpen: boolean;
}
