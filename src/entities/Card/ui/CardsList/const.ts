import type {
  IGameCardsValues,
  IPlayerCardsValues,
  ICardsTypeValues,
} from './ui/CardsList.interface';

import {
  GameCardTypes,
  PlayerCardTypes,
} from 'entities/Card/model/consts/enums';

import cls from './ui/CardsList.module.scss';

export const GAME_CARDS_VALUES: IGameCardsValues[] = [
  {
    key: 'disaster-game-cards',
    objectKey: 'disaster',
    title: 'Катастрофы',
    type: GameCardTypes.disaster,
  },
  {
    key: 'risk-game-cards',
    objectKey: 'risk',
    title: 'Угрозы',
    type: GameCardTypes.risk,
  },
  {
    key: 'bunker-game-cards',
    objectKey: 'bunker',
    title: 'Особенности бункера',
    type: GameCardTypes.bunker,
  },
];

export const PLAYER_CARDS_VALUES: IPlayerCardsValues[] = [
  {
    key: 'biology-player-cards',
    objectKey: 'biology',
    title: 'Биология',
    type: PlayerCardTypes.biology,
  },
  {
    key: 'health-player-cards',
    objectKey: 'health',
    title: 'Здоровье',
    type: PlayerCardTypes.health,
  },
  {
    key: 'professions-player-cards',
    objectKey: 'professions',
    title: 'Профессии',
    type: PlayerCardTypes.professions,
  },
  {
    key: 'hobby-player-cards',
    objectKey: 'hobby',
    title: 'Хобби',
    type: PlayerCardTypes.hobby,
  },
  {
    key: 'baggage-player-cards',
    objectKey: 'baggage',
    title: 'Багаж',
    type: PlayerCardTypes.baggage,
  },
  {
    key: 'facts-player-cards',
    objectKey: 'facts',
    title: 'Факты',
    type: PlayerCardTypes.facts,
  },
];

export const CARDS_TYPE_COLOR_VALUES: ICardsTypeValues = {
  BAGGAGE: cls.baggage,
  BIOLOGY: cls.biology,
  BUNKER: cls.bunker,
  DISASTER: cls.disaster,
  FACTS: cls.facts,
  HEALTH: cls.health,
  HOBBY: cls.hobby,
  PROFESSIONS: cls.professions,
  RISK: cls.risk,
};
