import type { RouteProps } from 'react-router-dom';

import { LoginPage } from 'pages/LoginPage';
import { CardsPage } from 'pages/CardsPage';
import { MenuPage } from 'pages/MenuPage';
import { NotFoundPage } from 'pages/NotFoundPage';
import { GamesPage } from 'pages/GamesPage';
import { GamePage } from 'pages/GamePage';
import { PlayersPage } from 'pages/PlayersPage';

import { AppRoutes, RoutePath } from 'shared/consts/router';

export type IRouteConfig = RouteProps & {
  isAuthOnly: boolean;
};

export const routeConfig: Record<AppRoutes, IRouteConfig> = {
  [AppRoutes.LOGIN]: {
    path: RoutePath.login,
    element: <LoginPage />,
    isAuthOnly: false,
  },
  [AppRoutes.MENU]: {
    path: RoutePath.menu,
    element: <MenuPage />,
    isAuthOnly: true,
  },
  [AppRoutes.NOT_FOUND]: {
    path: RoutePath.not_found,
    element: <NotFoundPage />,
    isAuthOnly: false,
  },
  [AppRoutes.CARDS]: {
    path: RoutePath.cards,
    element: <CardsPage />,
    isAuthOnly: true,
  },
  [AppRoutes.GAMES]: {
    path: RoutePath.games,
    element: <GamesPage />,
    isAuthOnly: true,
  },
  [AppRoutes.PLAYERS]: {
    path: RoutePath.players,
    element: <PlayersPage />,
    isAuthOnly: true,
  },
  [AppRoutes.GAME]: {
    path: `${RoutePath.game}:id`,
    element: <GamePage />,
    isAuthOnly: true,
  },
};
