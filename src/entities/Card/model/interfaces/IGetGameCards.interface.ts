import { GameCardTypes } from '../consts/enums';

export interface IGetGameCardsQuery {
  player_card_id: string;
}

export interface IGetGameCardsResult {
  id: string;
  name: string;
  description: string;
  type: GameCardTypes;
}
