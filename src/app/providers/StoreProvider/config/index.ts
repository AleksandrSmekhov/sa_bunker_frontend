import { configureStore } from '@reduxjs/toolkit';
import { setupListeners } from '@reduxjs/toolkit/query';

import { api } from 'shared/api';
import { cardsSlice, playersSlice, gamesSlice, gameSlice } from 'shared/slices';

const store = configureStore({
  reducer: {
    [api.reducerPath]: api.reducer,
    [cardsSlice.reducerPath]: cardsSlice.reducer,
    [playersSlice.reducerPath]: playersSlice.reducer,
    [gamesSlice.reducerPath]: gamesSlice.reducer,
    [gameSlice.reducerPath]: gameSlice.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({ serializableCheck: false }).concat(api.middleware),
});

setupListeners(store.dispatch);

export type RootState = ReturnType<typeof store.getState>;

export default store;
