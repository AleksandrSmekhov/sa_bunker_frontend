import { api } from 'shared/api';

import type {
  ICreateGameBody,
  ICreateGameResult,
} from './interfaces/ICreateGame.interface';
import type { IDeleteQuery } from './interfaces/IDelete.interface';
import type { IJoinGameBody } from './interfaces/IJoinGame.interface';

import {
  CREATE_GAME_URL,
  DELETE_GAME_URL,
  JOIN_GAME_URL,
  LEAVE_GAME_URL,
} from './consts/urls';
import { POST_METHOD, DELETE_METHOD } from 'shared/consts/requestsMethods';

const GameApi = api.injectEndpoints({
  endpoints: (build) => ({
    // POST
    createGame: build.mutation<ICreateGameResult, ICreateGameBody>({
      query: (body) => ({
        url: CREATE_GAME_URL,
        method: POST_METHOD,
        body,
      }),
    }),
    joinGame: build.mutation<any, IJoinGameBody>({
      query: (body) => ({
        url: JOIN_GAME_URL,
        method: POST_METHOD,
        body,
      }),
    }),

    // DELETE
    deleteGame: build.mutation<any, IDeleteQuery>({
      query: (query) => ({
        url: DELETE_GAME_URL,
        method: DELETE_METHOD,
        params: query,
      }),
    }),
    leaveGame: build.mutation<any, IDeleteQuery>({
      query: (query) => ({
        url: LEAVE_GAME_URL,
        method: DELETE_METHOD,
        params: query,
      }),
    }),
  }),
});

export const useCreateGame = GameApi.useCreateGameMutation;
export const useJoinGame = GameApi.useJoinGameMutation;

export const useDeleteGame = GameApi.useDeleteGameMutation;
export const useLeaveGame = GameApi.useLeaveGameMutation;
