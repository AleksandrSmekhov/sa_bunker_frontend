import type { GameCardTypes } from '../consts/enums';

export interface ICreateGameCardBody {
  name: string;
  description: string;
  type: GameCardTypes;
}
