import type { IGetPlayersResult } from 'entities/Player/model/interfaces/IGetPlayers.interface';

export const INITIAL_VALUES: IGetPlayersResult = [];

export const PLAYERS_SLICE_NAME = 'playersSlice';
