import { useMemo } from 'react';

import { useTypedSelector } from 'shared/lib/hooks';

import { CardInfo } from 'entities/Card';

import type { IPlayerItemProps } from './PlayerItem.interface';

import { CARDS_ICONS_VALUES } from '../const';

import cls from './PlayerItem.module.scss';

export const PlayerItem = ({ player }: IPlayerItemProps) => {
  const {
    game,
    players,
    player: user,
  } = useTypedSelector((state) => state.gameSlice);

  const [isOne, isGreyText, statusText] = useMemo(() => {
    if (!player.is_in_game) return [true, false, 'Выбыл'];

    if (game.round > 6) return [true, false, 'Выжил'];

    if (!game.is_voting) {
      return [
        true,
        true,
        game.current_player === player.number ? 'Ходит...' : '',
      ];
    }

    if (game.current_player === player.number)
      return [true, true, 'Голосует...'];

    if (!player.vote) return [true, true, ''];

    return [
      false,
      false,
      player.vote === user.id
        ? 'Вас'
        : players.find((foreign_player) => foreign_player.id === player.vote)
            ?.name,
    ];
  }, [player, game, players, user]);

  return (
    <details>
      <summary className={cls.info}>
        <p>{player.number + 1}</p>
        <p className={cls.name}>{player.name}</p>
        <div className={cls.icons}>
          {CARDS_ICONS_VALUES.map((values) => (
            <values.icon
              key={values.key}
              className={
                player.cards.find((card) => card.type === values.type)?.is_open
                  ? values.activeStyle
                  : values.inActiveStyle
              }
            />
          ))}
        </div>
        <div className={cls.status}>
          {isOne ? '' : <p className={cls.title}>Голос за:</p>}
          <p
            className={
              isGreyText
                ? cls.title
                : statusText === 'Выбыл'
                ? cls.red
                : cls.green
            }
          >
            {statusText}
          </p>
        </div>
      </summary>
      <div className={cls.cards}>
        {player.cards.map((card) => (
          <CardInfo
            key={card.id}
            description={card.description}
            isOpen={!player.is_in_game || card.is_open}
            name={card.name}
            type={card.type}
            interactions={card.interactions}
          />
        ))}
      </div>
    </details>
  );
};
