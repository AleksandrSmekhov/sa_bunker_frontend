import type { PlayerCardTypes } from '../consts/enums';

export interface ICreatePlayerCardBody {
  name: string;
  description: string;
  type: PlayerCardTypes;
}
