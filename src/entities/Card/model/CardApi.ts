import { api } from 'shared/api';

import type { ICreateGameCardBody } from './interfaces/ICreateGameCard.interface';
import type { ICreatePlayerCardBody } from './interfaces/ICreatePlayerCard.interface';
import type { ICreateInteractionBody } from './interfaces/ICreateInteraction.interface';
import type { IUpdateGameCardBody } from './interfaces/IUpdateGameCard.interface';
import type { IUpdatePlayerCardBody } from './interfaces/IUpdatePlayerCard.interface';
import type { IDeleteQuery } from './interfaces/IDelete.interface';
import type {
  IGetGameCardsResult,
  IGetGameCardsQuery,
} from './interfaces/IGetGameCards.interface';
import type {
  IGetPlayerCardsResult,
  IGetPlayerCardsQuery,
} from './interfaces/IGetPlayerCards.interface';
import type { IOpenCardBody } from './interfaces/IOpenCard.interface';

import {
  GET_GAME_CARDS_URL,
  GET_PLAYER_CARDS_URL,
  CREATE_GAME_CARD_URL,
  CREATE_PLAYER_CARD_URL,
  CREATE_INTERACTION_URL,
  UPDATE_GAME_CARD_URL,
  UPDATE_PLAYER_CARD_URL,
  DELETE_GAME_CARD_URL,
  DELETE_PLAYER_CARD_URL,
  DELETE_INTERACTION_URL,
  OPEN_CARD_URL,
} from './consts/urls';
import {
  POST_METHOD,
  DELETE_METHOD,
  PUT_METHOD,
  PATCH_METHOD,
} from 'shared/consts/requestsMethods';

const CardApi = api.injectEndpoints({
  endpoints: (build) => ({
    // GET
    getGameCards: build.query<IGetGameCardsResult[], IGetGameCardsQuery>({
      query: (query) => ({
        url: GET_GAME_CARDS_URL,
        params: query,
      }),
      providesTags: ['GameCards'],
    }),
    getPlayerCards: build.query<IGetPlayerCardsResult[], IGetPlayerCardsQuery>({
      query: (query) => ({
        url: GET_PLAYER_CARDS_URL,
        params: query,
      }),
      providesTags: ['PlayerCards'],
    }),

    // POST
    createGameCard: build.mutation<any, ICreateGameCardBody>({
      query: (body) => ({
        url: CREATE_GAME_CARD_URL,
        method: POST_METHOD,
        body,
      }),
      invalidatesTags: ['GameCards'],
    }),
    createPlayerCard: build.mutation<any, ICreatePlayerCardBody>({
      query: (body) => ({
        url: CREATE_PLAYER_CARD_URL,
        method: POST_METHOD,
        body,
      }),
      invalidatesTags: ['PlayerCards'],
    }),
    createCardsInteraction: build.mutation<any, ICreateInteractionBody>({
      query: (body) => ({
        url: CREATE_INTERACTION_URL,
        method: POST_METHOD,
        body,
      }),
      invalidatesTags: ['GameCards', 'PlayerCards'],
    }),

    // PUT
    updateGameCard: build.mutation<any, IUpdateGameCardBody>({
      query: (body) => ({
        url: UPDATE_GAME_CARD_URL,
        method: PUT_METHOD,
        body,
      }),
      invalidatesTags: ['GameCards'],
    }),
    updatePlayerCard: build.mutation<any, IUpdatePlayerCardBody>({
      query: (body) => ({
        url: UPDATE_PLAYER_CARD_URL,
        method: PUT_METHOD,
        body,
      }),
      invalidatesTags: ['PlayerCards'],
    }),

    // PATCH
    openCard: build.mutation<any, IOpenCardBody>({
      query: (body) => ({
        url: OPEN_CARD_URL,
        method: PATCH_METHOD,
        body,
      }),
    }),

    // DELETE
    deleteGameCard: build.mutation<any, IDeleteQuery>({
      query: (query) => ({
        url: DELETE_GAME_CARD_URL,
        method: DELETE_METHOD,
        params: query,
      }),
      invalidatesTags: ['GameCards'],
    }),
    deletePlayerCard: build.mutation<any, IDeleteQuery>({
      query: (query) => ({
        url: DELETE_PLAYER_CARD_URL,
        method: DELETE_METHOD,
        params: query,
      }),
      invalidatesTags: ['PlayerCards'],
    }),
    deleteCardInteraction: build.mutation<any, IDeleteQuery>({
      query: (query) => ({
        url: DELETE_INTERACTION_URL,
        method: DELETE_METHOD,
        params: query,
      }),
      invalidatesTags: ['GameCards', 'PlayerCards'],
    }),
  }),
});

export const useGetGameCards = CardApi.useGetGameCardsQuery;
export const useGetPlayerCards = CardApi.useGetPlayerCardsQuery;

export const useCreateGameCard = CardApi.useCreateGameCardMutation;
export const useCreatePlayerCard = CardApi.useCreatePlayerCardMutation;
export const useCreateCardsInteraction =
  CardApi.useCreateCardsInteractionMutation;

export const useUpdateGameCard = CardApi.useUpdateGameCardMutation;
export const useUpdatePlayerCard = CardApi.useUpdatePlayerCardMutation;

export const useOpenCard = CardApi.useOpenCardMutation;

export const useDeleteGameCard = CardApi.useDeleteGameCardMutation;
export const useDeletePlayerCard = CardApi.useDeletePlayerCardMutation;
export const useDeleteCardsInteraction =
  CardApi.useDeleteCardInteractionMutation;
