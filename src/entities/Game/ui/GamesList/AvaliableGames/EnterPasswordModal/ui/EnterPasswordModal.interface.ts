export interface IEnterPasswordModalProps {
  modalRef: React.RefObject<HTMLDialogElement>;
  setGameId: React.Dispatch<React.SetStateAction<string>>;
  gameId: string;
}
