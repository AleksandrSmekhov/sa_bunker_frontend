import { ReactComponent as Cards } from 'shared/assets/icons/cards.svg';
import { ReactComponent as Games } from 'shared/assets/icons/games.svg';
import { ReactComponent as Players } from 'shared/assets/icons/list.svg';
import { ReactComponent as Exit } from 'shared/assets/icons/exit.svg';

import type { IMenuItem } from './ui/MenuPage.interface';

import { RoutePath } from 'shared/consts/router';

import cls from './ui/MenuPage.module.scss';

export const MENU_ITEMS: IMenuItem[] = [
  {
    title: 'Карты',
    key: 'cards-menu-button',
    path: RoutePath.cards,
    icon: Cards,
    iconStyle: cls.svgStroke,
  },
  {
    title: 'Игра',
    key: 'games-menu-button',
    path: RoutePath.games,
    icon: Games,
    iconStyle: cls.svgFill,
  },
  {
    title: 'Игроки',
    key: 'players-menu-button',
    path: RoutePath.players,
    icon: Players,
    iconStyle: cls.svgPathStroke,
  },
  {
    title: 'Выход',
    key: 'exit-menu-button',
    path: RoutePath.login,
    icon: Exit,
    iconStyle: cls.svgPathStroke,
  },
];
