import type { IArchiveGame } from 'entities/Game/model/interfaces/IGetGames.interface';

export interface IArchiveGameItemProps {
  game: IArchiveGame;
  lineStyle: string;
}
