import type { IFlags } from './ui/AddCardItem.interface';
import type { IInputs } from '../CardForm/ui/CardForm.interface';

export const INITIAL_FLAGS: IFlags = {
  isDescriptionError: false,
  isNameError: false,
  isOpen: false,
};

export const INITIAL_INPUTS_VALUES: IInputs = {
  name: '',
  description: '',
};
