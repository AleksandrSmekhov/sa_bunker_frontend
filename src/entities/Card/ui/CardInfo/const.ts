import { ReactComponent as Baggage } from 'shared/assets/icons/baggage.svg';
import { ReactComponent as Biology } from 'shared/assets/icons/biology.svg';
import { ReactComponent as Bunker } from 'shared/assets/icons/bunker.svg';
import { ReactComponent as Disaster } from 'shared/assets/icons/disaster.svg';
import { ReactComponent as Facts } from 'shared/assets/icons/facts.svg';
import { ReactComponent as Health } from 'shared/assets/icons/health.svg';
import { ReactComponent as Hobby } from 'shared/assets/icons/hobby.svg';
import { ReactComponent as Profession } from 'shared/assets/icons/proffession.svg';
import { ReactComponent as Risk } from 'shared/assets/icons/risk.svg';

import type {
  ITypeTranslation,
  ITypeIcon,
  IIconStyle,
} from './ui/CardInfo.interface';

import cls from './ui/CardInfo.module.scss';

export const TYPES_TRANSLATIONS: ITypeTranslation = {
  BAGGAGE: 'Багаж',
  BIOLOGY: 'Биология',
  BUNKER: 'Особенность бункера',
  DISASTER: 'Катастрофа',
  FACTS: 'Факт',
  HEALTH: 'Здоровье',
  HOBBY: 'Хобби',
  PROFESSIONS: 'Профессия',
  RISK: 'Угроза',
};

export const TYPES_ICONS: ITypeIcon = {
  BAGGAGE: Baggage,
  BIOLOGY: Biology,
  BUNKER: Bunker,
  DISASTER: Disaster,
  FACTS: Facts,
  HEALTH: Health,
  HOBBY: Hobby,
  PROFESSIONS: Profession,
  RISK: Risk,
};

export const INACTIVE_ICON_STYLES: IIconStyle = {
  BAGGAGE: cls.inactiveFill,
  BIOLOGY: cls.inactiveFill,
  BUNKER: cls.inactiveFill,
  DISASTER: cls.inactiveStroke,
  FACTS: cls.inactiveStroke,
  HEALTH: cls.inactiveFill,
  HOBBY: cls.inactiveFill,
  PROFESSIONS: cls.inactiveFill,
  RISK: cls.inactiveFill,
};

export const ACTIVE_STYLES: IIconStyle = {
  BAGGAGE: cls.baggage,
  BIOLOGY: cls.biology,
  BUNKER: cls.bunker,
  DISASTER: cls.disaster,
  FACTS: cls.facts,
  HEALTH: cls.health,
  HOBBY: cls.hobby,
  PROFESSIONS: cls.profession,
  RISK: cls.risk,
};
