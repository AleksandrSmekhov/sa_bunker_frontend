import { ReactComponent as Gitlab } from 'shared/assets/icons/gitlab.svg';

import { SOURCE_LINKS_VALUES } from '../const';

import cls from './ProjectInfoBlock.module.scss';

export const ProjectInfoBlock = () => {
  return (
    <div className={cls.projectInfo}>
      {SOURCE_LINKS_VALUES.map((values) => (
        <div className={cls.infoLine} key={values.key}>
          <p>{values.title}:</p>
          <a target="_black" href={values.href}>
            <Gitlab />
            Исходный код
          </a>
        </div>
      ))}
      <a href="https://smekhov-alex.ru" className={cls.backButton}>
        ← На главную
      </a>
    </div>
  );
};
