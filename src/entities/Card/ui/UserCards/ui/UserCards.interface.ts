import type { PlayerCardTypes } from 'entities/Card/model/consts/enums';

export type ITypeIcon = Record<
  PlayerCardTypes,
  React.FC<React.SVGProps<SVGSVGElement>>
>;
export type IIconStyle = Record<PlayerCardTypes, string>;
export type IIsOpenCards = Record<number, boolean>;
