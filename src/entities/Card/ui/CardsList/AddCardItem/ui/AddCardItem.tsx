import { useState } from 'react';

import { ReactComponent as Plus } from 'shared/assets/icons/plus.svg';
import { CardForm } from '../../CardForm';

import type { IAddCardItemProps, IFlags } from './AddCardItem.interface';

import { INITIAL_FLAGS, INITIAL_INPUTS_VALUES } from '../const';

import cls from './AddCardItem.module.scss';

export const AddCardItem = ({ extraColor, type }: IAddCardItemProps) => {
  const [flags, setFlags] = useState<IFlags>(INITIAL_FLAGS);

  const handleOpen = () => {
    setFlags((prev) => ({ ...prev, isOpen: true }));
  };

  const handleReset = (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    event.preventDefault();
    setFlags(INITIAL_FLAGS);
  };

  return flags.isOpen ? (
    <div className={`${cls.addCard} ${extraColor}`}>
      <CardForm
        handleReset={handleReset}
        initialInputs={INITIAL_INPUTS_VALUES}
        type={type}
      />
    </div>
  ) : (
    <div
      className={`${cls.addCard} ${extraColor} ${cls.notOpened}`}
      onClick={handleOpen}
    >
      <div className={cls.plus}>
        <Plus />
      </div>
    </div>
  );
};
