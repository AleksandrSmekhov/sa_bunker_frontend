import { useNavigate } from 'react-router-dom';

import { ReactComponent as Enter } from 'shared/assets/icons/exit.svg';

import { getPlayersText } from 'shared/lib/funcs';

import type { IArchiveGameItemProps } from './ArchiveGameItem.interface';

import { RoutePath } from 'shared/consts/router';

import cls from './ArchiveGameItem.module.scss';

export const ArchiveGameItem = ({ game, lineStyle }: IArchiveGameItemProps) => {
  const navigate = useNavigate();

  const handleOpenGame = () => {
    navigate(`${RoutePath.game}${game.id}`);
  };

  return (
    <div className={lineStyle}>
      <p>{game.name}</p>
      <p className={cls.disaster}>{game.disaster}</p>
      <p className={cls.players}>
        {game.players} {getPlayersText(game.players)}
      </p>
      <button onClick={handleOpenGame}>
        <Enter />
      </button>
    </div>
  );
};
