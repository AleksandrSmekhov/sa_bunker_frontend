import { createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';

import type { IGetGamesResult } from 'entities/Game/model/interfaces/IGetGames.interface';

import { GAMES_SLICE_NAME, INITIAL_VALUES } from './const';

export const gamesSlice = createSlice({
  name: GAMES_SLICE_NAME,
  initialState: INITIAL_VALUES,
  reducers: {
    setData(_, { payload }: PayloadAction<IGetGamesResult>) {
      return payload;
    },
  },
});

export const gamesSliceActions = gamesSlice.actions;
