import { useMemo, useState, useEffect } from 'react';
import Select from 'react-select';

import type { SingleValue } from 'react-select';

import { useTypedSelector } from 'shared/lib/hooks';
import { useOpenCard } from 'entities/Card';
import { useVoteForPlayer } from 'entities/Player';

import type { IOption } from './SelectCardOrVote.interface';

import { TYPES_COLORS } from '../const';

import {
  CONTROL_STYLES,
  DROPDOWN_INDICATOR_STYLES,
  INDICATORS_CONTAINER_STYLES,
  MENU_LIST_STYLES,
  MENU_STYLES,
  OPTION_STYLES,
  PLACEHOLDER_STYLES,
  SINGLE_VALUE_STYLES,
  VALUE_CONTAINER_STYLES,
} from './styles/SelectStyles';
import cls from './styles/SelectCardOrVote.module.scss';

export const SelectCardOrVote = () => {
  const { player, game, players } = useTypedSelector(
    (state) => state.gameSlice
  );
  const options: IOption[] = useMemo(
    () =>
      game.is_voting
        ? players
            .filter((single_player) => single_player.is_in_game)
            .map((single_player) => ({
              label: single_player.name,
              value: single_player.id,
            }))
        : player.cards
            .filter((card) => !card.is_open)
            .map((card) => ({
              label: card.name,
              value: card.id,
              color: TYPES_COLORS[card.type],
            })),
    [game, player, players]
  );

  const [currentOption, setCurrentOption] = useState<IOption>(options[0]);
  const [openCard] = useOpenCard();
  const [vote] = useVoteForPlayer();
  useEffect(() => setCurrentOption(options[0]), [options]);

  const handleChange = (newValue: SingleValue<IOption>) => {
    newValue && setCurrentOption(newValue);
  };

  const handleOpenCard = () => {
    openCard({
      card_id: currentOption.value,
      game_id: game.id,
      player_id: player.id,
    })
      .unwrap()
      .then(() => ({}))
      .catch(() => ({}));
  };

  const handleVote = () => {
    vote({ game_id: game.id, player_id: player.id, vote: currentOption.value })
      .unwrap()
      .then(() => ({}))
      .catch(() => ({}));
  };

  if (game.current_player !== player.number || game.round > 6) return <></>;

  return (
    <div className={cls.selectBlock}>
      <p>
        {game.is_voting
          ? 'Выберете игрока за которого голосуете:'
          : 'Выберете карту, которую хотите открыть:'}
      </p>
      <div className={`${cls.select} ${game.is_voting ? cls.vote : cls.card}`}>
        <Select
          menuPlacement="top"
          isSearchable={false}
          isClearable={false}
          isMulti={false}
          value={currentOption}
          onChange={handleChange}
          options={options}
          styles={{
            control: CONTROL_STYLES,
            singleValue: SINGLE_VALUE_STYLES,
            valueContainer: VALUE_CONTAINER_STYLES,
            placeholder: PLACEHOLDER_STYLES,
            indicatorsContainer: INDICATORS_CONTAINER_STYLES,
            dropdownIndicator: DROPDOWN_INDICATOR_STYLES,
            menu: MENU_STYLES,
            menuList: MENU_LIST_STYLES,
            option: OPTION_STYLES,
          }}
        />
        <button
          className={cls.button}
          onClick={game.is_voting ? handleVote : handleOpenCard}
        >
          {game.is_voting ? 'Голосовать' : 'Открыть'}
        </button>
      </div>
    </div>
  );
};
