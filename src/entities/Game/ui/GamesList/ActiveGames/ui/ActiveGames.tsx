import { ActiveGameItem } from '../ActiveGameItem';

import type { IActiveGamesProps } from './ActiveGames.interface';

export const ActiveGames = ({ games, styles }: IActiveGamesProps) => {
  return (
    <div className={styles.block}>
      <h2 className={styles.header}>Текущие игры</h2>
      {games.map((game) => (
        <ActiveGameItem game={game} lineStyle={styles.game} key={game.id} />
      ))}
    </div>
  );
};
