import { useTypedSelector } from 'shared/lib/hooks';

import { ReactComponent as Skull } from 'shared/assets/icons/skull.svg';
import { PlayerItem } from '../PlayerItem';

import { IS_ADMIN_STORAGE_NAME } from 'shared/consts/storageNames';

import cls from './PlayersList.module.scss';

export const PlayersList = () => {
  const players = useTypedSelector((state) => state.playersSlice);
  const isAdmin = localStorage.getItem(IS_ADMIN_STORAGE_NAME);

  if (!players.length)
    return (
      <div className={cls.noPlayers}>
        <h2>Игроков не найдено...</h2>
        <Skull />
      </div>
    );

  return (
    <div className={cls.playersList}>
      <div
        className={`${cls.header} ${isAdmin ? cls.adminView : cls.playerView}`}
      >
        <p>№</p>
        <p>Имя</p>
        <p>Кол-во игр</p>
      </div>
      {players.map((player, index) => (
        <PlayerItem
          key={player.id}
          player={player}
          index={index + 1}
          isAdmin={!!isAdmin}
        />
      ))}
    </div>
  );
};
