import { PropsWithChildren } from 'react';

export interface IContentWindowProps extends PropsWithChildren {
  extraBlockWrapperClass?: string;
  extraBlockClass?: string;
  header?: string;
}
