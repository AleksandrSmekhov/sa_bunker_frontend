export const capitalizeString = (string: string) => {
  return string.replace(/(^|\s)\S/g, (letter) => letter.toUpperCase());
};
