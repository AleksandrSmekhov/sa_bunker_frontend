import { useState, useEffect } from 'react';
import Select from 'react-select';

import type { SingleValue } from 'react-select';

import { useGetGameCards, useCreateCardsInteraction } from 'entities/Card';

import { ReactComponent as Plus } from 'shared/assets/icons/plus.svg';
import { ReactComponent as Cross } from 'shared/assets/icons/cross.svg';
import { ReactComponent as Tick } from 'shared/assets/icons/tick.svg';

import type { IOption } from './AddPlayerCardInteraction.interface';
import type { IAddInteractionProps } from '../../ui/CardInteractions.interface';

import { OPTION_COLORS } from '../const';

import {
  SINGLE_VALUE_STYLES,
  CONTROL_STYLES,
  DROPDOWN_INDICATOR_STYLES,
  INDICATORS_CONTAINER_STYLES,
  INPUT_STYLES,
  MENU_LIST_STYLES,
  MENU_STYLES,
  OPTION_STYLES,
  PLACEHOLDER_STYLES,
  VALUE_CONTAINER_STYLES,
} from '../../ui/styles/SelectorStyles';
import cls from '../../ui/styles/AddInteraction.module.scss';

export const AddPlayerCardInteraction = ({
  cardId,
  type,
}: IAddInteractionProps) => {
  const [isOpened, setIsOpened] = useState<boolean>(false);
  const [options, setOptions] = useState<IOption[]>([]);
  const [currentOption, setCurrentOption] = useState<IOption>();

  const { data } = useGetGameCards(
    {
      player_card_id: cardId,
    },
    { skip: !isOpened }
  );
  const [createInteraction] = useCreateCardsInteraction();

  useEffect(() => {
    if (data && data.length) {
      return setOptions(
        data.map((card) => ({
          label: card.name,
          type: card.type,
          value: card.id,
        }))
      );
    }
    return setOptions([]);
  }, [data]);

  const handleOpen = () => {
    setIsOpened(true);
  };

  const handleClose = () => {
    setCurrentOption(undefined);
    setIsOpened(false);
  };

  const handleCurrentOption = (option: SingleValue<IOption>) => {
    if (option) setCurrentOption(option);
  };

  const handleCreate = () => {
    if (currentOption)
      createInteraction({
        type: type,
        game_card: currentOption.value,
        player_card: cardId,
      }).unwrap();
    handleClose();
  };

  return isOpened ? (
    <div className={cls.addSelect}>
      <Select
        options={options}
        placeholder="Выберите карту"
        value={currentOption}
        isSearchable={true}
        isClearable={false}
        defaultValue={null}
        noOptionsMessage={() => 'Нет доступных карт'}
        onChange={handleCurrentOption}
        styles={{
          control: (base) =>
            CONTROL_STYLES(
              base,
              currentOption && OPTION_COLORS[currentOption?.type]
            ),
          input: INPUT_STYLES,
          singleValue: SINGLE_VALUE_STYLES,
          valueContainer: VALUE_CONTAINER_STYLES,
          placeholder: PLACEHOLDER_STYLES,
          indicatorsContainer: INDICATORS_CONTAINER_STYLES,
          dropdownIndicator: DROPDOWN_INDICATOR_STYLES,
          menu: MENU_STYLES,
          menuList: MENU_LIST_STYLES,
          option: (base, props) =>
            OPTION_STYLES(base, OPTION_COLORS[props.data?.type!]),
        }}
      />
      <Tick className={cls.tick} onClick={handleCreate} />
      <Cross onClick={handleClose} className={cls.cross} />
    </div>
  ) : (
    <div className={cls.addButton} onClick={handleOpen}>
      <Plus />
    </div>
  );
};
