import { memo, useCallback, Suspense } from 'react';
import { Route, Routes } from 'react-router-dom';

import { NotFoundPage } from 'pages/NotFoundPage';
import { LoadingElement } from 'features/LoadingElement';
import { RequireAuth } from './RequireAuth';
import { Page } from 'features/Page';

import { routeConfig, type IRouteConfig } from './config';

const AppRouter = () => {
  const renderWithWrapper = useCallback((route: IRouteConfig) => {
    const element = (
      <Suspense fallback={<LoadingElement />}>{route.element}</Suspense>
    );

    return (
      <Route
        key={route.path}
        path={route.path}
        element={
          route.isAuthOnly ? <RequireAuth>{element}</RequireAuth> : element
        }
      />
    );
  }, []);

  return (
    <Routes>
      <Route element={<Page />} path="/">
        {Object.values(routeConfig).map(renderWithWrapper)}
      </Route>
      <Route element={<NotFoundPage />} path="*" />
    </Routes>
  );
};

export default memo(AppRouter);
