import { useState } from 'react';

import { ReactComponent as Gear } from 'shared/assets/icons/gear.svg';
import { CardForm } from '../../CardForm';
import { CardInteractions } from '../CardInteractions';

import type { ICardItemProps, IFlags } from './CardItem.interface';

import { CARDS_TYPE_TRANSLATION_VALUES, INITIAL_FLAGS } from '../const';

import cls from './CardItem.module.scss';

export const CardItem = ({ card, extraColor, isAdmin }: ICardItemProps) => {
  const [flags, setFlags] = useState<IFlags>(INITIAL_FLAGS);

  const handlePreventOpen = (
    event: React.SyntheticEvent<HTMLDetailsElement, Event>
  ) => {
    event.preventDefault();
  };

  const handleToggle = () => {
    setFlags((prev) => ({ ...prev, isOpen: !flags.isOpen }));
  };

  const handleReset = (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    event.preventDefault();
    setFlags((prev) => ({ ...prev, isInEdit: false }));
  };

  const handleInEdit = () => {
    setFlags((prev) => ({ ...prev, isInEdit: true }));
  };

  return (
    <details
      className={`${cls.cardInfo} ${flags.isOpen ? cls.opened : ''}`}
      open={flags.isOpen}
      onClick={handlePreventOpen}
    >
      <summary className={`${cls.card} ${extraColor}`}>
        {flags.isInEdit ? (
          <CardForm
            handleReset={handleReset}
            initialInputs={{ description: card.description, name: card.name }}
            type={card.type}
            cardId={card.id}
          />
        ) : (
          <div className={cls.cardContent}>
            <h4 className={cls.header}>
              {CARDS_TYPE_TRANSLATION_VALUES[card.type]}
              {isAdmin ? <Gear onClick={handleInEdit} /> : <></>}
            </h4>
            <div className={cls.descriptionWrapper} onClick={handleToggle}>
              <div className={cls.description}>
                {card.description.split('\n').map((line, index) => (
                  <p key={`descriptio-line-${index}`}>{line}</p>
                ))}
              </div>
            </div>
            <h4 className={cls.name} onClick={handleToggle}>
              {card.name}
            </h4>
          </div>
        )}
      </summary>
      <CardInteractions card={card} isAdmin={isAdmin} />
    </details>
  );
};
