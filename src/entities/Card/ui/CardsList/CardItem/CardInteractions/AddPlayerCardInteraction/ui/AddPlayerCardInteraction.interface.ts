import type { GameCardTypes } from 'entities/Card/model/consts/enums';

export interface IOption {
  label: string;
  value: string;
  type: GameCardTypes;
}

export type IOptionColors = Record<GameCardTypes, string>;
