import type { IGetCardsResult } from 'entities/Card/model/interfaces/IGetCards.interface';

export const INITIAL_VALUES: IGetCardsResult = {
  game_cards: {
    bunker: [],
    disaster: [],
    risk: [],
  },
  player_cards: {
    baggage: [],
    biology: [],
    facts: [],
    health: [],
    hobby: [],
    professions: [],
  },
};

export const CARDS_SLICE_NAME = 'cardsSlice';
