import { api } from 'shared/api';

import type { IDeleteQuery } from './interfaces/IDelete.interface';
import type { IReadyPlayerBody } from './interfaces/IReadyPlayer.interface';
import type { IVotePlayerBody } from './interfaces/IVotePlayer.interface';

import { DELETE_METHOD, PATCH_METHOD } from 'shared/consts/requestsMethods';
import {
  DELETE_PLAYER_URL,
  READY_PLAYER_URL,
  VOTE_PLAYER_URL,
} from './consts/urls';

const PlayerApi = api.injectEndpoints({
  endpoints: (build) => ({
    // PATCH
    readyPlayer: build.mutation<any, IReadyPlayerBody>({
      query: (body) => ({
        url: READY_PLAYER_URL,
        method: PATCH_METHOD,
        body,
      }),
    }),
    voteForPlayer: build.mutation<any, IVotePlayerBody>({
      query: (body) => ({
        url: VOTE_PLAYER_URL,
        method: PATCH_METHOD,
        body,
      }),
    }),

    // DELETE
    deletePlayer: build.mutation<any, IDeleteQuery>({
      query: (query) => ({
        url: DELETE_PLAYER_URL,
        method: DELETE_METHOD,
        params: query,
      }),
    }),
  }),
});

export const useReadyPlayer = PlayerApi.useReadyPlayerMutation;
export const useVoteForPlayer = PlayerApi.useVoteForPlayerMutation;
export const useDeletePlayer = PlayerApi.useDeletePlayerMutation;
