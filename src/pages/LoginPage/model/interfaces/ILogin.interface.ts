export interface ILoginBody {
  login: string;
}

export interface ILoginResult {
  id: string;
  name: string;
  is_admin: boolean;
}
