import type { IArchiveGame } from 'entities/Game/model/interfaces/IGetGames.interface';

export interface IArchiveGamesProps {
  games: IArchiveGame[];
  styles: Record<string, string>;
}
