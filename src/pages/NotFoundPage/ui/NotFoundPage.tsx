import { useNavigate } from 'react-router-dom';

import { ContentWindow } from 'features/ContentWindow';

import { RoutePath } from 'shared/consts/router';

import cls from './NotFoundPage.module.scss';

export const NotFoundPage = () => {
  const navigate = useNavigate();

  const handleOnMainPage = () => {
    navigate(RoutePath.menu);
  };

  return (
    <ContentWindow>
      <h2 className={cls.header}>Страница не найдена</h2>
      <p onClick={handleOnMainPage} className={cls.text}>
        На главную ⇒
      </p>
    </ContentWindow>
  );
};
