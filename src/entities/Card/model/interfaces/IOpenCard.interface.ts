export interface IOpenCardBody {
  card_id: string;
  player_id: string;
  game_id: string;
}
