import { AppRouter } from './providers/Router';

import './styles/index.css';

const App = () => {
  return <AppRouter />;
};

export default App;
