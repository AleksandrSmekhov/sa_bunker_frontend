import { GameCardTypes, PlayerCardTypes } from '../consts/enums';

interface IGameCardInteraction {
  game_link_card: string;
  id: string;
  name: string;
  description: string;
  type: PlayerCardTypes;
}

export const GAME_LINK_CARD_KEY = 'game_link_card';

interface IPalyerCardInteraction {
  player_link_card: string;
  id: string;
  name: string;
  description: string;
  type: GameCardTypes;
}

export const PLAYER_LINK_CARD_KEY = 'player_link_card';

export interface IGameCard {
  id: string;
  name: string;
  description: string;
  type: GameCardTypes;
  negative: IGameCardInteraction[];
  positive: IGameCardInteraction[];
}

export interface IPlayerCard {
  id: string;
  name: string;
  description: string;
  type: PlayerCardTypes;
  negative: IPalyerCardInteraction[];
  positive: IPalyerCardInteraction[];
}

export interface IGameCards {
  risk: IGameCard[];
  bunker: IGameCard[];
  disaster: IGameCard[];
}

export interface IPlayerCards {
  baggage: IPlayerCard[];
  biology: IPlayerCard[];
  facts: IPlayerCard[];
  health: IPlayerCard[];
  hobby: IPlayerCard[];
  professions: IPlayerCard[];
}

export interface IGetCardsResult {
  game_cards: IGameCards;
  player_cards: IPlayerCards;
}
