const PLAYER_URL = '/player/';

export const GET_PLAYER_URL = PLAYER_URL;
export const DELETE_PLAYER_URL = PLAYER_URL;

const PLAYER_READY_URL = '/player/ready/';

export const READY_PLAYER_URL = PLAYER_READY_URL;

const PLAYER_VOTE_URL = '/player/vote/';

export const VOTE_PLAYER_URL = PLAYER_VOTE_URL;
