import type {
  IGameCard,
  IPlayerCard,
} from 'entities/Card/model/interfaces/IGetCards.interface';

import type {
  GameCardTypes,
  PlayerCardTypes,
} from 'entities/Card/model/consts/enums';

type ICard = IGameCard | IPlayerCard;

export interface ICardItemProps {
  card: ICard;
  extraColor: string;
  isAdmin: boolean;
}

export type ICardsTypeValues = Record<GameCardTypes | PlayerCardTypes, string>;

export interface IFlags {
  isOpen: boolean;
  isInEdit: boolean;
}
